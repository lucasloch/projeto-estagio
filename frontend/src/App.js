import React, { useState, useEffect } from 'react';
import jwt_decode from 'jwt-decode';
import axios from 'axios';
import { BrowserRouter, Route, Routes, Outlet, Navigate, useNavigate } from "react-router-dom";
// Tela inicial para LOGIN
import Login from './components/Login';
// Telas do ADMIN
import TelaAdmin from './components/admins/Tela';
import Cadastro from './components/admins/Cadastro';
import MenuAdmin from './components/admins/Menu';
// Telas do GESTOR
import TelaColab from './components/colaborador/Tela';
import MenuColab from './components/colaborador/Menu';
import Ferias from './components/colaborador/Ferias';
import EquipeColab from './components/colaborador/EquipeColab';
// Telas do COLABORADOR
import TelaGestor from './components/gestor/Tela';
import Solicitacoes from './components/gestor/Solicitacoes';
import Equipe from './components/gestor/Equipes';
import FeriasGestor from './components/gestor/Ferias';
import Calendario from './components/gestor/Calendario';
import MinhasSolicitacoes from './components/gestor/MinhasSolicitacoes';
import Teste from './components/gestor/teste';
import Relatorios from './components/gestor/Relatorios';

function GestorRoute() {
  return (
    <div>
      <TelaGestor>
        <Outlet/>
      </TelaGestor>
    </div>
  );
  /*if (permissao == 'ADIMN') {
    return (
      <div>
        <TelaGestor>
          <Outlet/>
        </TelaGestor>
      </div>
    );
  } else if (permissao == "GESTOR") {
    <Navigate to="/gestor" replace={true} />
  } else if (permissao == "USER") {
    <Navigate to="/user" replace={true} />
  }*/
}
  
function AdminRoute() {
  return (
    <div>
      <TelaAdmin>
        <Outlet/>
      </TelaAdmin>
    </div>
  );
}
  
function ColabRoute() {
  return (
    <div>
      <TelaColab>
        <Outlet/>
      </TelaColab>
    </div>
  );
}

export default function App() {
  /*const [permissao, setPermissao] = useState(null);
  const conectado = localStorage.getItem('token');

  useEffect(() => {
    if (conectado) {
      const token = localStorage.getItem('token');
      const id = (jwt_decode(token)).usuario_id;
    
      axios.get(`http://localhost:3000/usuarios/${id}`)
      .then(response => response.data)
      .then((data) => {
        if (data.cargo == 'ADMIN') setPermissao('ADMIN');
        else if (data.e_gestor) setPermissao('GESTOR');
        else if (!data.e_gestor) setPermissao('USER');
      });
    } else {
      setPermissao(null);
    }
  })*/

  return (
    <div>
      <BrowserRouter>
        <Routes>
            <Route exact path="/" element={<Login/>} />
            <Route exact path="/test" element={<Teste/>} />
              
            <Route exact path="admin" element={<AdminRoute />}>
                <Route exact index path="menu" element={<MenuAdmin/>} />
                <Route exact path="menu/cadastro" element={<Cadastro/>} />
            </Route>
              
            <Route exact path="gestor" element={<GestorRoute />}>
                <Route exact index path="dashboard" element={<Calendario />} />
                <Route exact path="equipe" element={<Equipe/>} />
                <Route exact path="solicitacoes" element={<Solicitacoes/>} />
                <Route exact path="solicitacoes/ferias" element={<FeriasGestor/>} />
                <Route exact path="ferias" element={<MinhasSolicitacoes/>} />
                <Route exact path="relatorios" element={<Relatorios/>} />
            </Route>
              
            <Route exact path="user" element={<ColabRoute />}>
                <Route exact index path="menu" element={<MenuColab/>} />
                <Route exact path="menu/ferias" element={<Ferias/>} />
                <Route exact path="equipe" element={<EquipeColab/>} />
            </Route>
          </Routes>
      </BrowserRouter>  
    </div>
  );
}
