import React, { useEffect, useState } from "react";
import axios from 'axios';
import jwt_decode from "jwt-decode";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

export default function TabelaEquipe(props) {
  const [usuarios, setUsuarios] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    
    axios.get(`http://localhost:3000/usuarios/busca/${id}`)
      .then(response => response.data)
      .then((data) => {
        setUsuarios(data);
      });
  });

  const filtrado = usuarios.filter((saida) => {
    console.log(saida)
    if (props.input === '') {
      return saida;
    } else {
      return (saida.usuario_nome.toLowerCase().includes(props.input) || saida.usuario_cargo.toLowerCase().includes(props.input));
    }
  });

  return (
    <div>
      <Paper className="container">
        <Table>
          <TableHead>
            <TableRow>
              <TableCell><b>Matrícula</b></TableCell>
              <TableCell numeric><b>Nome</b></TableCell>
              <TableCell numeric><b>Setor</b></TableCell>
              <TableCell numeric><b>Cargo</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {filtrado.map(({ usuario_usuario_id, usuario_matricula, usuario_nome, usuario_setor, usuario_cargo }) => (
              <TableRow key={usuario_usuario_id}>
                <TableCell component="th" scope="row">
                  {usuario_matricula}
                </TableCell>
                <TableCell numeric>{usuario_nome}</TableCell>
                <TableCell numeric>{usuario_setor}</TableCell>
                <TableCell numeric>{usuario_cargo}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}
