import React, { useEffect, useState } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import Paper from "@material-ui/core/Paper";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { Typography } from '@material-ui/core';


export default function AcumuloFerias() {
  const [precisaFerias, setPrecisaFerias] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/usuarios/busca/tempo/${id}`)
      .then(response => response.data)
      .then((data) => {
        setPrecisaFerias(data[0]);
      });
  })

  return (
    <div>
      {console.log(precisaFerias)}
      {precisaFerias.ferias_acumuladas ? 
        <Paper className="container">
        <AppBar
          position="static"
          color="default"
          elevation={0}
          sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
        >
          <Toolbar>
            <Typography variant="h5" component="div" sx={{ flexGrow: 1 }}>
              Você está com férias acumuladas. Converse com seu gestor.
            </Typography>
          </Toolbar>
        </AppBar>
        </Paper>
      :
      <></>
      }
    </div>
)}
