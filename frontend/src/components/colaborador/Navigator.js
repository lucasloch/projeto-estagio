import * as React from 'react';
import axios from 'axios';
import jwt_decode from "jwt-decode";
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import HomeIcon from '@mui/icons-material/Home';
import GroupIcon from '@mui/icons-material/Group';
import LogoutIcon from '@mui/icons-material/Logout';
import logo from '../../logo3.png';
import { Link } from 'react-router-dom';

const categories = [
  {
    id: 'Colaborador',
    children: [
      { id: 'Menu', icon: <HomeIcon />, link: 'menu'},
      { id: 'Equipe', icon: <GroupIcon />, link: 'equipe' },
    ],
  },
  {
    id: 'Geral',
    children: [
      { id: 'Sair', icon: <LogoutIcon />, link: '/' }
    ],
  },
];

const item = {
  py: '2px',
  px: 3,
  color: '#black',
  '&:hover, &:focus': {
    bgcolor: '#black',
  },
};

const itemCategory = {
  boxShadow: '0 -1px 0 rgb(255,255,255,0.1) inset',
  py: 1.5,
  px: 3,
};

export default function Navigator(props) {
  const { ...other } = props;

  const [nome, setNome] = React.useState();
  const [cargo, setCargo] = React.useState();
  const [setor, setSetor] = React.useState();

  React.useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    console.log(id);
    axios.get(`http://localhost:3000/usuarios/${id}`)
      .then(response => response.data)
      .then((data) => {
        setNome(data[0].nome);
        setCargo(data[0].cargo);
        setSetor(data[0].setor);
      });
  });

  return (
    <Drawer variant="permanent" {...other}>
      <List disablePadding>
        <ListItem>
          <img src={logo} alt="Logo" style={{width:'16vw'}} />
        </ListItem>
        <ListItem sx={{ ...item, ...itemCategory }}>
          <ListItemText primary={nome} secondary={`${cargo} no setor de ${setor}`} />
        </ListItem>
        {categories.map(({ id, children }) => (
          <Box key={id} sx={{ bgcolor: '#ebedf0' }}>
            <ListItem sx={{ py: 2, px: 3 }}>
              <ListItemText sx={{ color: '#black' }}>{id}</ListItemText>
            </ListItem>
            {children.map(({ id: childId, icon, link }) => (
              childId == 'Sair' ?
              (<ListItem style={{ color: 'inherit', textDecoration: 'inherit'}} component={Link} to={link} disablePadding key={childId}>
                <ListItemButton sx={item}>
                  <ListItemIcon onClick={() => localStorage.removeItem("token")}>{icon}</ListItemIcon>
                  <ListItemText onClick={() => localStorage.removeItem("token")}>{childId}</ListItemText>
                </ListItemButton>
              </ListItem>)
              : 
              <ListItem style={{ color: 'inherit', textDecoration: 'inherit'}} component={Link} to={link} disablePadding key={childId}>
                <ListItemButton sx={item}>
                  <ListItemIcon>{icon}</ListItemIcon>
                  <ListItemText>{childId}</ListItemText>
                </ListItemButton>
              </ListItem>
            ))}
            <Divider sx={{ mt: 2 }} />
          </Box>
        ))}
      </List>
    </Drawer>
  );
}