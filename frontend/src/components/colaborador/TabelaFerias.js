import React, { useEffect, useState } from "react";
import axios from 'axios';
import jwt_decode from "jwt-decode";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Alert } from "@mui/material";

const statusMap = {
    'Aberto': 'info',
    'Aprovado': 'success',
    'Recusado': 'error'
};

export default function TabelaFeriasColab() {
  const [solicitacoes, setSolicitacoes] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    
    axios.get(`http://localhost:3000/solicitacoes/pedidos/${id}`)
      .then(response => response.data)
      .then((data) => {
        setSolicitacoes(data);
      });
  });

  function formatDate(data) {
    var date = new Date(data);
    var dateString = [
      date.getUTCFullYear() , 
      ("0" + (date.getUTCMonth()+1)).slice(-2), 
      ("0" + date.getUTCDate()).slice(-2)
    ].join("-");
    return dateString.split("-").reverse().join("/");
  }

  return (
    <div>
      <Paper className="container">
        <Table>
          <TableHead>
            <TableRow>
              <TableCell><b>Data do Pedido</b></TableCell>
              <TableCell><b>Início Férias</b></TableCell>
              <TableCell><b>Fim Férias</b></TableCell>
              <TableCell><b>Estado</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {solicitacoes.map(({ solicitacao_solicitacao_id, solicitacao_solicitacao_data, solicitacao_ferias_inicio, solicitacao_ferias_fim, solicitacao_solicitacao_estado }) => (
              <TableRow key={solicitacao_solicitacao_id}>
                <TableCell component="th" scope="row">
                  {formatDate(solicitacao_solicitacao_data)}
                </TableCell>
                <TableCell>{formatDate(solicitacao_ferias_inicio)}</TableCell>
                <TableCell>{formatDate(solicitacao_ferias_fim)}</TableCell>
                <TableCell>
                  <Alert severity={statusMap[solicitacao_solicitacao_estado]}>
                    {solicitacao_solicitacao_estado}
                  </Alert>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}
