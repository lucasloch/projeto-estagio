import React, { useEffect, useState } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import Paper from "@material-ui/core/Paper";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { Typography } from '@material-ui/core';


export default function Solicitar11Meses() {
  const [dados, setDados] = useState([]);
  const [passou11meses, setPassou11meses] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/usuarios/feriasvencidas/${id}`)
      .then(response => response.data)
      .then((data) => {
        setDados(data[0]);
        setPassou11meses(data[0].passou11meses);
      });
  })

  return (
    <div>
      {console.log(passou11meses)}
      {passou11meses ? 
        <Paper className="container">
        <AppBar
          position="static"
          color="default"
          elevation={0}
          sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
        >
          <Toolbar>
            <Typography variant="h5" component="div" sx={{ flexGrow: 1 }}>
              Seu último período de férias foi a quase 11 meses. Converse com seu gestor.
            </Typography>
          </Toolbar>
        </AppBar>
        </Paper>
      :
      <></>
      }
    </div>
)}
