import React, { useState, useEffect } from "react";
import axios from 'axios';
import jwt_decode from "jwt-decode";
import { makeStyles } from "@material-ui/core/styles";
import { InputLabel, MenuItem, Select, FormControl } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { useNavigate } from "react-router-dom";
import AppBar from '@mui/material/AppBar';
import Typography from "@material-ui/core/Typography";
import Alert from '@mui/material/Alert';

const useStyles = makeStyles((theme) => ({
    form: {
      padding: '2%',
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(1)
    },
  }));

export default function Ferias() {
  const classes = useStyles();

  const navigate = useNavigate();

  function handleClick() {
    navigate("/user/menu");
  }
  
  useEffect(() => {
    setDataFimFerias(addDays());
  })

  function addDays() {
    // Essa função obtém a data de término das férias
    let data = new Date(dataInicioFerias);
    data.setDate(data.getDate() + (qtdDias+1));
    return formatDate(data);
  }

  function formatDate(data = new Date) {
    // Essa função obtém a data de hoje e a formata
    // o valor de retorno irá setar a data máxima permitida data de contratação
    
    function padTo2Digits(num) {
      return num.toString().padStart(2, '0');
    }
    const hojeFormat = [
      data.getFullYear(),
      padTo2Digits(data.getMonth() + 1),
      padTo2Digits(data.getDate()),
    ].join('-');
    return hojeFormat;
  }

  const [dataInicioFerias, setDataInicioFerias] = useState(null);
  const [qtdDias, setQtdDias] = useState(null);
  const [dataFimFerias, setDataFimFerias] = useState(null);
  const [salario, setSalario] = useState(false);
  const [observacao, setObservacao] = useState(null);
  const [gestor_id, setGestorId] = useState(null);
  const [status, setStatus] = useState(null);
  const [contrato, setContrato] = useState();

  const handleSubmit = (e) => {
		e.preventDefault();
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.post("http://localhost:3000/solicitacoes/", {
      ferias_inicio: dataInicioFerias,
      quantidade_dias: qtdDias,
      salario: salario,
      observacao: observacao,
      usuario_id: id,
      gestor_id : gestor_id
    })
    .then((response) => {
      console.log(response.statusText);
      setStatus(response.statusText);
      sendMail();
    });
	};

  function sendMail() {
    axios.post("http://127.0.0.1:8000/solicitar/", {
      nome: nome,
      email: emailGestor,
      inicio_ferias: dataInicioFerias,
      qtd_dias: qtdDias,
      fim_ferias: dataFimFerias,
      observacao: observacao
    })
    .then((response) => {
      console.log(response);
    });
	};

  const [nome, setNome] = useState();
  useEffect (() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    console.log(id)
    
    axios.get(`http://localhost:3000/usuarios/${id}`)
      .then(response => response.data)
      .then((data) => {
        setContrato(data[0].contrato);
        setNome(data[0].nome);
        setGestorId(data[0].gestor_id);
      });
  });

  const [emailGestor, setEmailGestor] = useState();
  useEffect (() => {    
    axios.get(`http://localhost:3000/usuarios/${gestor_id}`)
      .then(response => response.data)
      .then((data) => {
        setEmailGestor(data[0].gmail);
      });
  });

  function verificaPedido() {
    if (status == 'Created') {
      return <Alert severity="success"><b>Pedido enviado. Clique em voltar ou mude página.</b></Alert>
    }
  }
  console.log(gestor_id);

  useEffect(() => {
    verificaPedido();
  })

  const [opcoesDias, setOpcoesDias] = useState([5, 10, 15, 20, 30]);
  useEffect (() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    
    axios.get(`http://localhost:3000/solicitacoes/saldo/${id}`)
      .then(response => response.data)
      .then((data) => {
        if (data[0].length > 3) {
          setOpcoesDias([5, 10, 15, 20, 30]);
        } else if ((data[0].dias_usados == 15) && (!data[0].teve_15dias)) {
          setOpcoesDias([15]);
        } else if ((data[0].dias_usados == 10) && (!data[0].teve_15dias)) {
          setOpcoesDias([5, 15, 20]);
        } else {
          setOpcoesDias([5, 10, 15, 20, 30].filter(x => x <= (30 - data[0].dias_usados)));
        }
      });
  });
  
  return (
    <Paper sx={{ maxWidth: 936, margin: 'auto', overflow: 'hidden' }}>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)', alignItems: 'center' }}
      >
      <Typography variant="h4" component="div" sx={{ flexGrow: 1 }}>
        Solicitar Férias
      </Typography>
      </AppBar>
      <form className={classes.form} noValidate onSubmit={handleSubmit}>
          <Grid container spacing={2}>
          {contrato == 'CLT' ?
          <>
          <Grid item xs={12} sm={4}>
            <TextField
              type="date"
              required
              style={{ width: "100%" }}
              variant="outlined"
              name="dataInicioFerias"
              id="dataInicioFerias"
              label="Data de Início"
              InputLabelProps={{ shrink: true, required: true }}
              InputProps={{inputProps: { min: formatDate()}, max: "2099-12-31" }}
              onChange={ e=>setDataInicioFerias(e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl
              required
              variant="outlined"
              margin={"1"}
              style={{ width: "100%" }}
            >
              <InputLabel id="inputQtdDias">Dias de Férias</InputLabel>
              <Select
                required
                style={{ width: "100%" }}
                variant="outlined"
                labelId="inputQtdDias"
                label={"Quantidade de Dias"}
                onChange={ e=>setQtdDias(e.target.value)}
              >
              {opcoesDias.map((dia) => (
                <MenuItem value={dia}>
                  {dia}
                </MenuItem>
              ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={4}>
            <FormControl
              required
              variant="outlined"
              margin={"1"}
              style={{ width: "100%" }}
            >
              <InputLabel id="inputSalario">Solicitar 13º Salário</InputLabel>
              <Select
                required
                style={{ width: "100%" }}
                variant="outlined"
                labelId="inputSalario"
                label={"Solicitar 13º Salário"}
                onChange={ e=>setSalario(e.target.value)}
              >
                <MenuItem value={true}>Sim</MenuItem>
                <MenuItem value={false}>Não</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          </>
          :
          <>
            <Grid item xs={12} sm={6}>
              <TextField
                type="date"
                required
                style={{ width: "100%" }}
                variant="outlined"
                name="dataInicioFerias"
                id="dataInicioFerias"
                label="Data de Início"
                InputLabelProps={{ shrink: true, required: true }}
                InputProps={{inputProps: { min: formatDate()}, max: "2099-12-31" }}
                onChange={ e=>setDataInicioFerias(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl
                required
                variant="outlined"
                margin={"1"}
                style={{ width: "100%" }}
              >
                <InputLabel id="inputQtdDias">Dias de Férias</InputLabel>
                <Select
                  required
                  style={{ width: "100%" }}
                  variant="outlined"
                  labelId="inputQtdDias"
                  label={"Quantidade de Dias"}
                  onChange={ e=>setQtdDias(e.target.value)}
                >
                {opcoesDias.map((dia) => (
                  <MenuItem value={dia}>
                    {dia}
                  </MenuItem>
                ))}
                </Select>
              </FormControl>
            </Grid>
          </>
          }
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                name="observacao"
                label="Observação"
                id="observacao"
                onInput={ e=>setObservacao(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                style={{
                  backgroundColor: "#A8DE1F",
                  color: "#FFF"
                }}
                type="submit"
                fullWidth
                variant="contained"
                onClick={verificaPedido()}
              >
                Solicitar
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                style={{
                  backgroundColor: "#FFF"
                }}
                onClick={handleClick}
                type="button"
                fullWidth
                variant="contained"
              >
                Voltar
              </Button>
            </Grid>
          </Grid>
        </form>
        <div>{verificaPedido()}</div>
    </Paper>
  );
}