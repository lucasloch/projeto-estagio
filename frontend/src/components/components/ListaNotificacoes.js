import React from "react";
import solicitacoes from "./solicitacoes.json";
import './lista.css';

export default function ListaNotificacoes(props) {
  function formatDate(data) {
    return data.split("-").reverse().join("/");
  }

  return (
    <table id="customers">
      <tr>
        <th id="th1">Data do Pedido</th>
        <th id="th2">Início das Férias</th>
        <th id="th3">Fim das Férias</th>
        <th id="th4">13º Salário</th>
        <th id="th5">Observação</th>
        <th id="th6">Estado do Pedido</th>
        <th id="th7">Motivo</th>
      </tr>
      {solicitacoes.map((item) => (
        <tr>
          <td headers="th1">{formatDate(item.data_pedido)}</td>
          <td headers="th2">{formatDate(item.inicio_ferias)}</td>
          <td headers="th3">{formatDate(item.fim_ferias)}</td>
          <td headers="th4">{item.salario ? "Sim" : "Não"}</td>
          <td headers="th5">{item.observacao}</td>
          <td headers="th6">{item.estado_pedido}</td>
          <td headers="th7">{item.motivo_recusa}</td>
        </tr>
      ))}
    </table>
  )
}
