import React from "react";
import usuarios from "./usuarios.json";
import './lista.css';

export default function ListaPesquisa(props) {
  const filtrado = usuarios.filter((saida) => {
    if (props.input === '') {
      return saida;
    } else if (isNaN(props.input)) {
      return (saida.nome.toLowerCase().includes(props.input) || saida.setor.toLowerCase().includes(props.input));
    } else {
      return saida.matricula.includes(props.input);
    }
  })

  return (
    <table id="customers">
      <tr>
        <th id="th1">Matrícula</th>
        <th id="th2">Nome Completo</th>
        <th id="th3">Contrato</th>
        <th id="th4">Setor</th>
        <th id="th5">Cargo</th>
      </tr>
      {filtrado.map((item) => (
        <tr>
          <td headers="th1">{item.matricula}</td>
          <td headers="th2">{item.nome}</td>
          <td headers="th3">{item.tipo_contrato}</td>
          <td headers="th4">{item.setor}</td>
          <td headers="th5">{item.tipo_acesso}</td>
        </tr>
      ))}
    </table>
  )
}
