import React, { useEffect, useState } from "react";
import axios from 'axios';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button } from "@material-ui/core";

export default function Tabela(props) {
  const [usuarios, setUsuarios] = useState([]);

  function gets() {
    axios.get("http://localhost:3000/usuarios/")
      .then(response => response.data)
      .then((data) => {
        setUsuarios(data)
      });
  }
  useEffect(()=>{
    gets();
  },[])

  function deleteHandler(id) {
    axios.delete(`http://localhost:3000/usuarios/${id}`)
      .then(response => {
        console.log("Deletado com sucesso.")
      })
  }

  const filtrado = usuarios.filter((saida) => {
    if (props.input === '') {
      return saida;
    } else {
      return (saida.matricula.includes(props.input) || saida.nome.toLowerCase().includes(props.input) || saida.setor.toLowerCase().includes(props.input));
    }
  });

  return (
    <div>
      <Paper className="container">
        <Table>
          <TableHead>
            <TableRow>
              <TableCell><b>Matrícula</b></TableCell>
              <TableCell numeric><b>Nome</b></TableCell>
              <TableCell numeric><b>Setor</b></TableCell>
              <TableCell numeric><b>Cargo</b></TableCell>
              <TableCell numeric><b>Contrato</b></TableCell>
              <TableCell numeric><b>Ação</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {filtrado.map(({ usuario_id, matricula, nome, setor, cargo, contrato }) => (
              <TableRow key={usuario_id}>
                <TableCell component="th" scope="row">
                  {matricula}
                </TableCell>
                <TableCell numeric>{nome}</TableCell>
                <TableCell numeric>{setor}</TableCell>
                <TableCell numeric>{cargo}</TableCell>
                <TableCell numeric>{contrato}</TableCell>
                <TableCell>
                  <Button onClick={() => deleteHandler(usuario_id)}
                    style={{
                      backgroundColor: "#cf0026",
                      color: "#FFF"
                    }}
                    fullWidth
                    variant="contained"
                    >Remover</Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}
