import * as React from 'react';
import { Link } from 'react-router-dom';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import HomeIcon from '@mui/icons-material/Home';
import LogoutIcon from '@mui/icons-material/Logout';
import logo from '../../logo3.png';

const categories = [
  {
    id: 'Admin',
    children: [
      { id: 'Menu', icon: <HomeIcon />, link: 'menu' }
    ],
  },
  {
    id: 'Geral',
    children: [
      { id: 'Sair', icon: <LogoutIcon />, link: '/' }
    ],
  },
];

const item = {
  py: '2px',
  px: 3,
  color: '#black',
  '&:hover, &:focus': {
    bgcolor: '#black',
  },
};

const itemCategory = {
  boxShadow: '0 -1px 0 rgb(255,255,255,0.1) inset',
  py: 1.5,
  px: 3,
};

export default function Navigator(props) {
  const { ...other } = props;

  return (
    <Drawer variant="permanent" {...other}>
      <List disablePadding>
        <ListItem>
          <img src={logo} alt="Logo" style={{width:'16vw'}} />
        </ListItem>
        {categories.map(({ id, children }) => (
          <Box key={id}>
            <ListItem sx={{ py: 2, px: 3 }}>
              <ListItemText sx={{ color: '#black' }}>{id}</ListItemText>
            </ListItem>
            {children.map(({ id: childId, icon, link }) => (
              childId == 'Sair' ?
              (<ListItem style={{ color: 'inherit', textDecoration: 'inherit'}} component={Link} to={link} disablePadding key={childId}>
                <ListItemButton sx={item}>
                  <ListItemIcon onClick={() => localStorage.removeItem("token")}>{icon}</ListItemIcon>
                  <ListItemText onClick={() => localStorage.removeItem("token")}>{childId}</ListItemText>
                </ListItemButton>
              </ListItem>)
              : 
              <ListItem disablePadding key={childId}>
                <ListItemButton component={Link} to={link} sx={item}>
                  <ListItemIcon>{icon}</ListItemIcon>
                  <ListItemText>{childId}</ListItemText>
                </ListItemButton>
              </ListItem>
            ))}
            <Divider sx={{ mt: 2 }} />
          </Box>
        ))}
      </List>
    </Drawer>
  );
}