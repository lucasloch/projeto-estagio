import * as React from 'react';
import { Link } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import SearchIcon from '@mui/icons-material/Search';
import Tabela from './Tabela';

export default function MenuAdmin() {
  const [textoBusca, setTextoBusca] = React.useState();
  
  const inputHandler = (e) => {
    if (isNaN(e)) {
      var minusculo = e.target.value.toLowerCase();
      setTextoBusca(minusculo);
    } else {
      setTextoBusca(e);
    }
  };

  return (
    <Paper sx={{ maxWidth: 936, margin: 'auto', overflow: 'hidden' }}>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
      >
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <SearchIcon color="inherit" sx={{ display: 'block' }} />
            </Grid>
            <Grid item xs>
              <TextField
                fullWidth
                placeholder="Busque por nome, matrícula ou setor"
                InputProps={{
                  disableUnderline: true,
                  sx: { fontSize: 'default' },
                }}
                variant="standard"
                onChange={inputHandler}
              />
            </Grid>
            <Grid item>
              <Button style={{ background: '#44af42' }} variant="contained">
                <Link to="cadastro" style={{ color: 'inherit', textDecoration: 'inherit'}}>
                  Cadastro
                </Link>
              </Button>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Tabela input={textoBusca}/>
    </Paper>
  );
}