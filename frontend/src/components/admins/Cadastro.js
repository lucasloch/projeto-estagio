import React, { useState, useEffect } from "react";
import axios from 'axios';
import { makeStyles } from "@material-ui/core/styles";
import { Autocomplete } from "@mui/material";
import { InputLabel, MenuItem, Select, FormControl } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { useNavigate } from "react-router-dom";
import AppBar from '@mui/material/AppBar';
import Typography from "@material-ui/core/Typography";

const useStyless = makeStyles((theme) => ({
    form: {
      padding: '2%',
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(1)
    },
  }));

export default function Cadastro() {
  const classes = useStyless();
  const [setores, setSetores] = useState([])
  const [gestores, setGestores] = useState([])

  function gets() {
    axios.get("http://localhost:3000/usuarios/areas")
      .then(response => response.data)
      .then((data) => {
        setSetores(data)
      });
    axios.get("http://localhost:3000/usuarios/gestores")
    .then(response => response.data)
    .then((data) => {
        setGestores(data)
    });
  }
  useEffect(()=>{
    gets();
  },[])

  const navigate = useNavigate();
  
  function handleClick() {
    navigate("/admin/menu");
  }
  
  function formatDate() {
    // Essa função obtém a data de hoje e a formata
    // o valor de retorno irá setar a data máxima permitida data de contratação
    const hoje = new Date();
  
    function padTo2Digits(num) {
      return num.toString().padStart(2, '0');
    }
    const hojeFormat = [
      hoje.getFullYear(),
      padTo2Digits(hoje.getMonth() + 1),
      padTo2Digits(hoje.getDate()),
    ].join('-');
    return hojeFormat;
  }
  
  const [matricula, setMatricula] = useState(null);
  const [contrato, setContrato] = useState(null);
  const [nome, setNome] = useState(null);
  const [dataContrato, setDataContrato] = useState(null);
  const [e_gestor, setEGestor] = useState(null);
  const [setor, setSetor] = useState(null);
  const [gestor, setGestor] = useState(null);
  const [cargo, setCargo] = useState(null);
  const [email, setEmail] = useState(null);
  const [gmail, setGmail] = useState(null);
  
  /*function handleSubmit(event) {
    // Como ainda não conectei back os valores serão exibidos em um alert para mostrar que foram obtidos
    event.preventDefault();
    alert(`Matrícula: ${matricula}\nContrato: ${contrato}\nNome: ${nome}\Contrato: ${contrato}\nNível: ${e_gestor}
Data de Contratação: ${dataContrato}\nSetor: ${setor}\nGestor: ${gestor}\nEmail: ${email}\nGmail: ${gmail}`);
  }*/

  const handleSubmit = (e) => {
		e.preventDefault();
    axios.post("http://localhost:3000/usuarios/cadastro", {
      matricula: matricula,
      contrato: contrato,
      nome: nome,
      e_gestor: e_gestor,
      setor: setor,
      cargo: cargo,
      email: email,
      gmail: gmail,
      data_contrato: dataContrato,
      nomeGestor: gestor
    })
    .then((response) => {
      console.log(response);
    });
	};

  return (
    <Paper sx={{ maxWidth: 936, margin: 'auto', overflow: 'hidden' }}>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)', alignItems: 'center' }}
      >
      <Typography variant="h4" component="div" sx={{ flexGrow: 1 }}>
        Realizar Cadastro
      </Typography>
      </AppBar>
      <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="matricula"
                name="matricula"
                variant="outlined"
                required
                fullWidth
                id="matricula"
                label="Matrícula"
                autoFocus
                onInput={ e=>setMatricula(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl
                required
                variant="outlined"
                margin={"1"}
                style={{ width: "100%" }}
              >
                <InputLabel id="inputContrato">Tipo de Contrato</InputLabel>
                <Select
                  defaultValue = ""
                  required
                  style={{ width: "100%" }}
                  variant="outlined"
                  labelId="inputContrato"
                  label={"Tipo de Contrato"}
                  onChange={ e=>setContrato(e.target.value)}
                >
                  <MenuItem value={"CLT"}>
                    CLT
                  </MenuItem>
                  <MenuItem value={"PJ"}>
                    PJ
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="nome"
                label="Nome Completo"
                name="nome"
                autoComplete="nome"
                onInput={ e=>setNome(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl
                required
                variant="outlined"
                margin={"1"}
                style={{ width: "100%" }}
              >
                <InputLabel id="inputAcesso">Nível do Cargo</InputLabel>
                <Select
                  defaultValue = ""
                  required
                  style={{ width: "100%" }}
                  variant="outlined"
                  labelId="inputAcesso"
                  label={"Tipo de Acesso"}
                  onChange={ e=>setEGestor(e.target.value)}
                >
                  <MenuItem value={true}>
                    Gestor
                  </MenuItem>
                  <MenuItem value={false}>
                    Cooperador
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                type="date"
                required
                style={{ width: "100%" }}
                variant="outlined"
                name="dataContrato"
                id="dataContrato"
                label="Data de Contratação"
                InputLabelProps={{ shrink: true, required: true }}
                InputProps={{inputProps: { min: "1967-08-15", max: formatDate()} }}
                onInput={ e=>setDataContrato(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <Autocomplete
                id="setor"
                freeSolo
                options={setores.map(setor => setor.setor)}
                renderInput={(params) => 
                    <TextField {...params}
                    variant="outlined"
                    label="Setor" />}
                onSelect={ e=>setSetor(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <Autocomplete
                id="gestor"
                freeSolo
                options={gestores.map(gestor => gestor.nome)}
                renderInput={(params) => 
                    <TextField {...params}
                    variant="outlined"
                    label="Gestor" />}
                onSelect={ e=>setGestor(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <Autocomplete
                id="cargo"
                freeSolo
                options={["Dev", "Estagiário", "Designer"]}
                renderInput={(params) => 
                    <TextField {...params}
                    variant="outlined"
                    label="Cargo" />}
                onSelect={ e=>setCargo(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                fullWidth
                name="email"
                label="Email Institucional"
                id="email"
                onInput={ e=>setEmail(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                fullWidth
                name="gmail"
                label="Gmail (se houver)"
                id="gmail"
                onInput={ e=>setGmail(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                style={{
                  backgroundColor: "#A8DE1F",
                  color: "#FFF"
                }}
                type="submit"
                fullWidth
                variant="contained"
                onClick={console.log('Foi')}
              >
                Cadastrar
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                style={{
                  backgroundColor: "#FFF"
                }}
                onClick={handleClick}
                type="button"
                fullWidth
                variant="contained"
              >
                Voltar
              </Button>
            </Grid>
          </Grid>
        </form>
    </Paper>
  );
}