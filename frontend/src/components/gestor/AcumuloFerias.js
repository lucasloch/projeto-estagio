import React, { useEffect, useState } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import Paper from "@material-ui/core/Paper";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { Typography } from '@material-ui/core';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";


export default function AcumuloFerias() {
  const [precisaFerias, setPrecisaFerias] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/usuarios/busca/ferias/${id}`)
      .then(response => response.data)
      .then((data) => {
        setPrecisaFerias(data);
      });
  })
  const [precisaFeriasFiltrado, setPrecisaFeriasFiltrado] = useState([]) ;
  useEffect(() => {
    console.log(precisaFerias.filter(x => x.ferias_acumuladas))
    setPrecisaFeriasFiltrado(precisaFerias.filter(x => x.ferias_acumuladas));
  });
  
  function formatDate(data) {
    var date = new Date(data);
    var dateString = [
      date.getUTCFullYear() , 
      ("0" + (date.getUTCMonth()+1)).slice(-2), 
      ("0" + date.getUTCDate()).slice(-2)
    ].join("-");
    return dateString.split("-").reverse().join("/");
  }

  return (
    <div>
      {console.log(precisaFerias)}
      {console.log(precisaFeriasFiltrado > 0)}
      {precisaFeriasFiltrado.length > 0 ? 
        <>
        <Paper className="container">
        <AppBar
          position="static"
          color="default"
          elevation={0}
          sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
        >
          <Toolbar>
            <Typography variant="h5" component="div" sx={{ flexGrow: 1 }}>
              Usuários com férias acumuladas:
            </Typography>
          </Toolbar>
        </AppBar>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell><b>Nome</b></TableCell>
              <TableCell numeric><b>Última férias</b></TableCell>
              <TableCell numeric><b>Início Período Aquisitivo</b></TableCell>
              <TableCell numeric><b>Fim Período Aquisitivo</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {precisaFeriasFiltrado.map(({ usuario_id, nome, ultima_ferias, inicio_aquisitivo, fim_aquisitivo }) => (
              <TableRow key={usuario_id}>
                <TableCell component="th" scope="row">
                  {nome}
                </TableCell>
                <TableCell numeric>{formatDate(ultima_ferias)}</TableCell>
                <TableCell numeric>{formatDate(inicio_aquisitivo)}</TableCell>
                <TableCell numeric>{formatDate(fim_aquisitivo)}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        </Paper>
        </>
      :
      <></>
      }
    </div>
)}
