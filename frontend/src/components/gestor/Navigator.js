import * as React from 'react';
import axios from 'axios';
import jwt_decode from "jwt-decode";
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Dashboard from '@mui/icons-material/Dashboard';
import GroupIcon from '@mui/icons-material/Group';
import MessageIcon from '@mui/icons-material/Message';
import ContactMailIcon from '@mui/icons-material/ContactMail';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import Badge from '@mui/material/Badge';
import logo from '../../logo3.png';
import { Link } from 'react-router-dom';

const categories = [
  {
    id: 'Gestor',
    children: [
      { id: 'Dashboard', icon: <Dashboard />, link: 'dashboard'},
      { id: 'Equipe', icon: <GroupIcon />, link: 'equipe' },
      { id: 'Solicitações', icon: <ContactMailIcon />, link: 'solicitacoes' },
      { id: 'Minhas Solicitações', icon: <MessageIcon />, link: 'ferias' },
      { id: 'Emitir Relatórios', icon: <MenuBookIcon />, link: 'relatorios' }
    ],
  },
  {
    id: 'Geral',
    children: [
      { id: 'Sair', icon: <LogoutIcon />, link: '/' }
    ],
  },
];

const item = {
  py: '2px',
  px: 3,
  color: '#black',
  '&:hover, &:focus': {
    bgcolor: '#black',
  },
};

const itemCategory = {
  boxShadow: '0 -1px 0 rgb(255,255,255,0.1) inset',
  py: 1.5,
  px: 3,
};

export default function Navigator(props) {
  const { ...other } = props;

  const verifyID = (e) => {
    if (e != 'Solicitações') {
      return true;
    }
    return false;
  };

  const [nome, setNome] = React.useState();
  const [cargo, setCargo] = React.useState();
  const [setor, setSetor] = React.useState();

  React.useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/usuarios/${id}`)
      .then(response => response.data)
      .then((data) => {
        setNome(data[0].nome);
        setCargo(data[0].cargo);
        setSetor(data[0].setor);
      });
  });

  const [solicitacoes, setSolicitacoes] = React.useState([]);

  React.useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/solicitacoes/aberto/${id}`)
      .then(response => response.data)
      .then((data) => {
        setSolicitacoes(data)
      });
  });

  return (
    <Drawer variant="permanent" {...other}>
      <List disablePadding>
        <ListItem>
          <img src={logo} alt="Logo" style={{width:'16vw'}} />
        </ListItem>
        <ListItem sx={{ ...item, ...itemCategory }}>
          <ListItemText primary={nome} secondary={`${cargo} no setor de ${setor}`} />
        </ListItem>
        {categories.map(({ id, children }) => (
          <Box key={id}>
            <ListItem sx={{ py: 2, px: 3 }}>
              <ListItemText sx={{ color: '#black' }}>{id}</ListItemText>
            </ListItem>
            {children.map(({ id: childId, icon, link }) => (
              childId == 'Sair' ?
              (<ListItem style={{ color: 'inherit', textDecoration: 'inherit'}} component={Link} to={link} disablePadding key={childId}>
                <ListItemButton sx={item}>
                  <ListItemIcon onClick={() => localStorage.removeItem("token")}>{icon}</ListItemIcon>
                  <ListItemText onClick={() => localStorage.removeItem("token")}>{childId}</ListItemText>
                </ListItemButton>
              </ListItem>)
              : verifyID(childId) ? 
              (<ListItem style={{ color: 'inherit', textDecoration: 'inherit'}} component={Link} to={link} disablePadding key={childId}>
                <ListItemButton sx={item}>
                  <ListItemIcon>{icon}</ListItemIcon>
                  <ListItemText>{childId}</ListItemText>
                </ListItemButton>
              </ListItem>)
              : (<ListItem style={{ color: 'inherit', textDecoration: 'inherit'}} component={Link} to={link} disablePadding key={childId}>
                <ListItemButton sx={item}>
                  <ListItemIcon><Badge badgeContent={solicitacoes.length} color="primary">{icon}</Badge></ListItemIcon>
                  <ListItemText>{childId}</ListItemText>
                </ListItemButton>
              </ListItem>)
            ))}
            <Divider sx={{ mt: 2 }} />
          </Box>
        ))}
      </List>
    </Drawer>
  );
}