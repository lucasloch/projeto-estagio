import React, { useState, useEffect } from 'react';
import axios from "axios";
import jwt_decode from "jwt-decode";
import { Link } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import { Typography } from '@material-ui/core';
import TabelaFerias from './TabelaFerias';

export default function MinhasSolicitacoes() {
  const [textoBusca, setTextoBusca] = React.useState();
  
  const inputHandler = (e) => {
    if (isNaN(e)) {
      var minusculo = e.target.value.toLowerCase();
      setTextoBusca(minusculo);
    } else {
      setTextoBusca(e);
    }
  };

  const [gestor, setGestor] = useState('');
  const [tem1ano, setTem1Ano] = useState('');
  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/usuarios/${id}`)
      .then(response => response.data)
      .then((data) => {
        setGestor(data[0].gestor_id !== 0)
        setTem1Ano(data[0].tem1ano)
      });
  });
  console.log(gestor);
  console.log(tem1ano);

  const [tamanhoInfo, setTamanhoInfo] = useState();
  const [info, setInfo] = useState();
  useEffect (() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    
    axios.get(`http://localhost:3000/solicitacoes/saldo/${id}`)
    .then((response) => {
      console.log(Object.keys(response.data[0]).length)
      if (Object.keys(response.data[0]).length == 4) { 
        setTamanhoInfo(4);
        setInfo(0)
      } else {
        setTamanhoInfo(response.data[0].length);
        setInfo(response.data[0].dias_usados)
      };
    });
  });
  console.log(tamanhoInfo);

  return (
    <Paper sx={{ maxWidth: 936, margin: 'auto', overflow: 'hidden' }}>
      {console.log(gestor)}
      {console.log(tamanhoInfo)}
      {console.log(tem1ano)}
      {console.log(info)}
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
      >
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs>
              <Typography variant="h5" component="div" sx={{ flexGrow: 1 }}>
                Solicitações de Férias
              </Typography>
            </Grid>
            <Grid item>
            {((tem1ano) && (gestor) && ((tamanhoInfo == 4) || (info < 30))) ?
                <Button style={{ background: '#44af42' }} variant="contained">
                  <Link to="ferias" style={{ color: 'inherit', textDecoration: 'inherit'}}>
                    Solicitar
                  </Link>
                </Button>
              :
                <Button style={{ background: '#90D58F' }} variant="contained" disabled>
                  <Link to="ferias" style={{ color: 'inherit', textDecoration: 'inherit'}}>
                    Solicitar
                  </Link>
                </Button>
              }
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <TabelaFerias/>
    </Paper>
  );
}