import React, { useState, useEffect } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Alert from '@mui/material/Alert';

const statusMap = {
  'Aberto': 'info',
  'Aprovado': 'success',
  'Recusado': 'error'
};

export default function TabelaEquipe(props) {
  const [solicitacoes, setSolicitacoes] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/solicitacoes/resolvido/${id}`)
      .then(response => response.data)
      .then((data) => {
        setSolicitacoes(data)
      });
  });

  function formatDate(data) {
    var date = new Date(data);
    var dateString = [
      date.getUTCFullYear() , 
      ("0" + (date.getUTCMonth()+1)).slice(-2), 
      ("0" + date.getUTCDate()).slice(-2)
    ].join("-");
    return dateString.split("-").reverse().join("/");
  }

  const filtrado = solicitacoes.filter((saida) => {
    if (props.input == '') {
      return saida;
    } else {
      return (saida.solicitacao_solicitacao_estado.toLowerCase().includes(props.input) || saida.usuario_nome.toLowerCase().includes(props.input));
    }
  });
  
  return (
    <div>
      <Paper className="container">
        <Table>
          <TableHead>
            <TableRow>
              <TableCell><b>Data do Pedido</b></TableCell>
              <TableCell><b>Nome</b></TableCell>
              <TableCell><b>Início Férias</b></TableCell>
              <TableCell><b>Fim Férias</b></TableCell>
              <TableCell><b>Observação</b></TableCell>
              <TableCell><b>Estado</b></TableCell>
              <TableCell><b>Mensagem</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {filtrado.map(n => (
              <TableRow key={n.solicitacao_solicitacao_id}>
                <TableCell component="th" scope="row">
                  {formatDate(n.solicitacao_solicitacao_data)}
                </TableCell>
                <TableCell>{n.usuario_nome}</TableCell>
                <TableCell>{formatDate(n.solicitacao_ferias_inicio)}</TableCell>
                <TableCell>{formatDate(n.solicitacao_ferias_fim)}</TableCell>
                <TableCell>{n.solicitacao_observacao}</TableCell>
                <TableCell>
                  <Alert icon={false} severity={statusMap[n.solicitacao_solicitacao_estado]}>
                    {n.solicitacao_solicitacao_estado}
                  </Alert>
                </TableCell>
                <TableCell>{n.solicitacao_mensagem}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
}
