import React, { useEffect, useState } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { Button, Grid } from "@mui/material";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { Typography } from '@material-ui/core';

const useStyless = makeStyles((theme) => ({
  form: {
    padding: '2%',
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
}));

export default function Relatorios() {
  const classes = useStyless();

  const [id, setId] = useState();
  const [nome, setNome] = useState();
  const [email, setEmail] = useState();
  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/usuarios/${id}`)
      .then(response => response.data)
      .then((data) => {
        setId(data[0].usuario_id);
        setNome(data[0].nome);
        setEmail(data[0].gmail);
      });
  });

  const handleSubmit = (e) => {
		e.preventDefault();
    axios.post("http://127.0.0.1:8000/relatorio", {
      id: id,
      nome: nome,
      email: email
    })
    .then((response) => {
      console.log(response);
    });
	};

  return (
    <div>
      <Paper sx={{ maxWidth: 936, margin: 'auto', overflow: 'hidden' }}>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
      >
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs>
              <Typography variant="h5" component="div" sx={{ flexGrow: 1 }}>
                Emitir Relatórios
              </Typography>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              O relatório conterá os seguintes dados:
              <ul>
                <li>Matrícula</li>
                <li>Nome</li>
                <li>Período Aquisitivo</li>
                <li>Status</li>
                <li>Dias Utilizados</li>
                <li>Saldo Total</li>
              </ul>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                style={{
                  backgroundColor: "#158f42",
                  color: "#FFF",
                  fontWeight: 'bold'
                }}
                type="submit"
                fullWidth
                variant="contained"
                onClick={console.log('Foi')}
              >
                Emitir
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <div style={{textAlign: 'center', padding: 6}}>Os relatórios são enviados para o seu Gmail.</div>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </div>
  );
}

/*

      <Grid container spacing={2} alignItems="center">
        <Grid item xs={12} sm={4}>
          <Button fullWidth variant="outlined" style={{ background: '#158f42' }}>
            <Link to="ferias" style={{ color: '#FFF', textDecoration: 'none' }}>
              Histórico de solicitações (todas)
            </Link>
          </Button>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Button fullWidth variant="outlined" style={{ background: '#158f42' }}>
            <Link to="ferias" style={{ color: '#FFF', textDecoration: 'none' }}>
              Solicitações desse ano
            </Link>
          </Button>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Button fullWidth variant="outlined" style={{ background: '#158f42' }}>
            <Link to="ferias" style={{ color: '#FFF', textDecoration: 'none' }}>
              Suas solicitações
            </Link>
          </Button>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Button fullWidth variant="outlined" style={{ background: '#158f42' }}>
            <Link to="ferias" style={{ color: '#FFF', textDecoration: 'none' }}>
              <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                <b>Histórico de solicitações</b>
              </Typography>
              <ul>
                <li><b>5 dias</b></li>
                <li><b>10 dias</b></li>
                <li><b>15 dias</b></li>
                <li><b>20 dias</b></li>
                <li><b>30 dias</b></li>
              </ul>
            </Link>
          </Button>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Button fullWidth variant="outlined" style={{ background: '#158f42' }}>
            <Link to="ferias" style={{ color: '#FFF', textDecoration: 'none' }}>
              Solicitações desse ano
            </Link>
          </Button>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Button fullWidth variant="outlined" style={{ background: '#158f42' }}>
            <Link to="ferias" style={{ color: '#FFF', textDecoration: 'none' }}>
              Suas solicitações
            </Link>
          </Button>
        </Grid>
      </Grid>
*/