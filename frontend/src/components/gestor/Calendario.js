import React, { useEffect, useState } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import Paper from "@material-ui/core/Paper";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { Typography } from '@material-ui/core';
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import 'moment/locale/pt-br'
import "react-big-calendar/lib/css/react-big-calendar.css";
import { Button, Grid } from "@mui/material";
import AcumuloFerias from "./AcumuloFerias";

moment.locale('pt-br')
const localizer = momentLocalizer(moment)

const cores = {
  5:  "#363537",
  10: "#EF2D56",
  15: "#ED7D3A",
  20: "#8CD867",
  30: "#2FBF71",
};

/*const cores = {
  5:  "#0078FF",
  10: "#00EB43",
  15: "#FFEE05",
  20: "#EB6400",
  30: "#FF00CA",
};*/

export default function Calendario() {
  const [eventos, setEventos] = useState();
  const [vazio, setVazio] = useState();

  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/solicitacoes/aprovado/${id}`)
      .then(response => response.data)
      .then((data) => {
        setEventos(data);
        setVazio(data.length);
      });
  })

  return (
    <div>
      <AcumuloFerias/>
      <Paper className="container">
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
      >
        <Toolbar>
          <Typography variant="h5" component="div" sx={{ flexGrow: 1 }}>
            Dashboard
          </Typography>
        </Toolbar>
      </AppBar>
        <div style={{padding: 10}}>
          {vazio == 0 ? <div>No momento não há períodos de férias agendados.</div>
          :
          <>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={12} md>
              <Button fullWidth style={{ background: cores['5'], color: 'white' }}><b>5 dias</b></Button>
            </Grid>
            <Grid item xs={12} md>
              <Button fullWidth style={{ background: cores['10'], color: 'white' }}><b>10 dias</b></Button>
            </Grid>
            <Grid item xs={12} md>
              <Button fullWidth style={{ background: cores['15'], color: 'white' }}><b>15 dias</b></Button>
            </Grid>
            <Grid item xs={12} md>
              <Button fullWidth style={{ background: cores['20'], color: 'white' }}><b>20 dias</b></Button>
            </Grid>
            <Grid item xs={12} md>
              <Button fullWidth style={{ background: cores['30'], color: 'white' }}><b>30 dias</b></Button>
            </Grid>
          </Grid>
          </>
          }
        </div>
        <Calendar
          views={['month', 'agenda']} 
          localizer={localizer}
          events={eventos}
          popup={true}
          startAccessor="start"
          endAccessor="end"
          style={{ height: '100vh' }} 
          eventPropGetter={
            (event) => {
              let newStyle = {
                color: 'white',
                fontWeight: 'bold',
                borderRadius: "10px",
                border: "none",
              };
        
              if (event.qtd in cores){
                newStyle.backgroundColor = cores[event.qtd]
              }
        
              return {
                className: "",
                style: newStyle
              };
            }
          }
        />
      </Paper>
  </div>
)}
