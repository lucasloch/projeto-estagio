import React, { useState, useEffect } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button, Typography } from "@mui/material";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { InputLabel, MenuItem, Select, FormControl } from "@material-ui/core";
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';


const statusMap = {
  'Aberto': 'info',
  'Aprovado': 'success',
  'Recusado': 'error'
};

export default function TabelaEquipe(props) {
  const [solicitacoes, setSolicitacoes] = useState([]);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/solicitacoes/aberto/${id}`)
      .then(response => response.data)
      .then((data) => {
        setSolicitacoes(data)
      });
  });

    /*function mudarEstado(sol_id, acao) {
      for( var i = 0; i < solicitacoes.length; ++i ) {
        if( sol_id == solicitacoes[i]['solicitacao_id'] ) {
          solicitacoes[i]['estado_pedido'] = acao ;
        }
      }
    }*/

  function formatDate(data) {
    var date = new Date(data);
    var dateString = [
      date.getUTCFullYear() , 
      ("0" + (date.getUTCMonth()+1)).slice(-2), 
      ("0" + date.getUTCDate()).slice(-2)
    ].join("-");
    return dateString.split("-").reverse().join("/");
  }

  const filtrado = solicitacoes.filter((saida) => {
    if (props.input == '') {
      return saida;
    } else {
      return (saida.usuario_nome.toLowerCase().includes(props.input));
    }
  });

  function findSolicitacao(solicitacao) {
    return solicitacao.solicitacao_solicitacao_id === id_solicitacao;
  }

  const [open, setOpen] = useState(false);
  const [mensagem, setMensagem] = useState('');
  const [permissao, setPermissao] = useState('');
  const [id_solicitacao, setIdSolicitacao] = useState(null);
  const [solicitacaoAtual, setSolicitacaoAtual] = useState(null);
  
  function handOpen(id) {
    setIdSolicitacao(id);
    handleClickOpen();
  }

  useEffect(() => {
    setSolicitacaoAtual(solicitacoes.find(findSolicitacao));
  })

  function handleClickOpen() {
    setOpen(true);
  };

  function handleClose() {
    setOpen(false);
  };

  const handleSubmit = (e) => {
		e.preventDefault();
    axios.patch(`http://localhost:3000/solicitacoes/responder/${id_solicitacao}`, {
      solicitacao_estado: permissao,
      mensagem: mensagem
    })
    .then((response) => {
      sendMail();
      handleClose();
    });
  }
  
  function sendMail() {
    axios.post("http://127.0.0.1:8000/aprovar/", {
      email: solicitacaoAtual.usuario_gmail,
      data_pedido: solicitacaoAtual.solicitacao_solicitacao_data,
      inicio_ferias: solicitacaoAtual.solicitacao_ferias_inicio,
      qtd_dias: solicitacaoAtual.solicitacao_quantidade_dias,
      fim_ferias: solicitacaoAtual.solicitacao_ferias_fim,
      observacao: solicitacaoAtual.solicitacao_observacao,
      resposta: permissao,
      mensagem: mensagem
    })
    .then((response) => {
      console.log(response);
    });
	};

  return (
    <div>
      <Paper className="container">
        {solicitacoes.length > 0
        ?
        <Table>
          <TableHead>
            <TableRow>
              <TableCell><b>Data do Pedido</b></TableCell>
              <TableCell><b>Nome</b></TableCell>
              <TableCell><b>Início Férias</b></TableCell>
              <TableCell><b>Fim Férias</b></TableCell>
              <TableCell><b>Observação</b></TableCell>
              <TableCell><b>Ação</b></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {filtrado.map(n => (
              <TableRow key={n.solicitacao_solicitacao_id}>
                <TableCell component="th" scope="row">
                  {formatDate(n.solicitacao_solicitacao_data)}
                </TableCell>
                <TableCell>{n.usuario_nome}</TableCell>
                <TableCell>{formatDate(n.solicitacao_ferias_inicio)}</TableCell>
                <TableCell>{formatDate(n.solicitacao_ferias_fim)}</TableCell>
                <TableCell>{n.solicitacao_observacao}</TableCell>
                <TableCell>
                  <Button onClick={() => handOpen(n.solicitacao_solicitacao_id)} style={{ background: '#158f42' }} variant="contained">Responder</Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        :
        <Typography variant="h6" component="div" sx={{ textAlign: 'center' }}>
          Não há solicitações no momento.
        </Typography>
        }
          
        <form id="myForm" onSubmit={handleSubmit}>
        <Dialog open={open} onClose={handleClose} fullScreen={fullScreen}>
      <DialogTitle>
        <Typography variant="h5" align="center">Resposta</Typography>
      </DialogTitle>
      <DialogContent>
        {solicitacaoAtual ?
        <DialogContentText>
          <b>Nome:</b> {solicitacaoAtual.usuario_nome}<br/>
          <b>Dias de Férias:</b> {solicitacaoAtual.solicitacao_quantidade_dias}<br/>
          <b>Início:</b> {formatDate(solicitacaoAtual.solicitacao_ferias_inicio)}<br/>
          <b>Fim:</b> {formatDate(solicitacaoAtual.solicitacao_ferias_fim)}<br/>
          <b>Observação:</b> {solicitacaoAtual.solicitacao_observacao}<br/><br/>
        </DialogContentText>
        :
        <div>a</div>}
          <FormControl
              required
              variant="outlined"
              margin={"1"}
              style={{ width: "100%" }}
            >
              <InputLabel id="inputPermissao">Permissão</InputLabel>
              <Select
                defaultValue = ""
                required
                style={{ width: "100%" }}
                variant="outlined"
                labelId="inputPermissao"
                label={"Resposta"}
                onChange={ e=>setPermissao(e.target.value) }
              >
                <MenuItem value={'Aprovado'}>
                  Sim
                </MenuItem>
                <MenuItem value={'Recusado'}>
                  Não
                </MenuItem>
              </Select>
            </FormControl>
        <TextField
          autoFocus
          variant="outlined"
          margin="normal"
          id="name"
          label="Mensagem"
          type="mensagem"
          fullWidth
          multiline
          maxRows={4}
          onChange={ e=>setMensagem(e.target.value) }
        />
      </DialogContent>
      <DialogActions>
        <Button type="submit" form="myForm" style={{ background: '#44af42' }} variant="contained">Responder</Button>
        <Button onClick={handleClose} style={{ color: 'black' }}>Cancelar</Button>
      </DialogActions>
    </Dialog>
      </form>
      </Paper>
    </div>
  );
}
