import React, { useState, useEffect } from 'react';
import axios from "axios";
import jwt_decode from "jwt-decode";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import SearchIcon from '@mui/icons-material/Search';
import Typography from '@mui/material/Typography';
import TabelaSol from './TabelaSol';
import TabelaHistorico from './TabelaHistorico';

export default function Solicitacoes() {
  const [textoBusca, setTextoBusca] = useState();
  
  const inputHandler = (e) => {
    if (isNaN(e)) {
      var minusculo = e.target.value.toLowerCase();
      setTextoBusca(minusculo);
    } else {
      setTextoBusca(e);
    }
  };

  const [gestor, setGestor] = useState('');
  const [tem1ano, setTem1Ano] = useState('');
  useEffect(() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    axios.get(`http://localhost:3000/usuarios/${id}`)
      .then(response => response.data)
      .then((data) => {
        setGestor(data[0].gestor_id !== 0)
        setTem1Ano(data[0].tem1ano)
      });
  });
  console.log(gestor);
  console.log(tem1ano);

  const [tamanhoInfo, setTamanhoInfo] = useState();
  const [info, setInfo] = useState();
  useEffect (() => {
    const token = localStorage.getItem('token');
    const id = (jwt_decode(token)).usuario_id;
    
    axios.get(`http://localhost:3000/solicitacoes/saldo/${id}`)
    .then((response) => {
      console.log(Object.keys(response.data[0]).length)
      if (Object.keys(response.data[0]).length == 4) { 
        setTamanhoInfo(4);
        setInfo(0)
      } else {
        setTamanhoInfo(response.data[0].length);
        setInfo(response.data[0].dias_usados)
      };
    });
  });
  console.log(tamanhoInfo);

  return (
    <Paper sx={{ maxWidth: 936, margin: 'auto', overflow: 'hidden' }}>
      {console.log(gestor)}
      {console.log(tamanhoInfo)}
      {console.log(tem1ano)}
      {console.log(info)}
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
      >
        <Typography gutterBottom variant="h4" component="div" sx={{ m: 2 }}>
          Solicitações em Aberto
        </Typography>
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <SearchIcon color="inherit" sx={{ display: 'block' }} />
            </Grid>
            <Grid item xs>
              <TextField
                fullWidth
                placeholder="Busque por nome"
                InputProps={{
                  disableUnderline: true,
                  sx: { fontSize: 'default' },
                }}
                variant="standard"
                onChange={inputHandler}
              />
            </Grid>
            <Grid item>
              {((tem1ano) && (gestor) && ((tamanhoInfo == 4) || (info < 30))) ?
                <Button style={{ background: '#44af42' }} variant="contained">
                  <Link to="ferias" style={{ color: 'inherit', textDecoration: 'inherit'}}>
                    Solicitar
                  </Link>
                </Button>
              :
                <Button style={{ background: '#90D58F' }} variant="contained" disabled>
                  <Link to="ferias" style={{ color: 'inherit', textDecoration: 'inherit'}}>
                    Solicitar
                  </Link>
                </Button>
              }
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <TabelaSol input={textoBusca}/>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: '1px solid rgba(0, 0, 0, 0.12)' }}
      >
        <Typography gutterBottom variant="h4" component="div" sx={{ m: 2 }}>
          Histórico
        </Typography>
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <SearchIcon color="inherit" sx={{ display: 'block' }} />
            </Grid>
            <Grid item xs>
              <TextField
                fullWidth
                placeholder="Busque por nome ou estado"
                InputProps={{
                  disableUnderline: true,
                  sx: { fontSize: 'default' },
                }}
                variant="standard"
                onChange={inputHandler}
              />
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <TabelaHistorico input={textoBusca}/>
    </Paper>
  );
}