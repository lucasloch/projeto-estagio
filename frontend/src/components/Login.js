import React, { useState, useEffect } from "react";
import jwt_decode from "jwt-decode";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { Alert, CssBaseline } from "@mui/material";
import logo from '.././logo4.png';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import bg from '../bg.png'

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundColor:"#158f42",
    //backgroundImage: `url(${bg})`,
    background:`linear-gradient(#38B768,transparent)`,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  size: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20,
    borderColor: "#000"
  },
  paper: {
    margin: theme.spacing(2, 6),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  imgLogo: {
    width: "100%",
    margin: theme.spacing(0)
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    color: 'white',
    fontWeight: 'bold'
  }
}));


export default function Login() {
  // classes de CSS
  const classes = useStyles();
  
  const [matricula, setMatricula] = useState('');
  const [senha, setSenha] = useState('');
  const [erroLogin, setErroLogin] = useState(false);
  const [permissao, setPermissao] = useState();

  /*function handleSubmit(event) {
    // Como ainda não conectei back os valores serão exibidos em um alert para mostrar que foram obtidos
    event.preventDefault();
    alert(`Matrícula: ${matricula}\nRegime: ${senha}`);
  }*/
  function handleSubmit(e) {
    e.preventDefault();
    axios.post(`http://localhost:3000/usuarios/login`, {
      matricula: matricula,
      senha: senha
    })
    .then((response) => {
      if (response.data.token) {
        localStorage.setItem("token", JSON.stringify(response.data.token));
        console.log(jwt_decode(response.data.token));
        setErroLogin(false);
        
        let decoded = jwt_decode(response.data.token);
        axios.get(`http://localhost:3000/usuarios/${decoded.usuario_id}`)
          .then(response => response.data)
          .then((data) => {
            console.log(data[0])
            if (data[0].cargo == 'ADMIN') setPermissao('ADMIN');
            else if (data[0].e_gestor) setPermissao('GESTOR');
            else setPermissao('COLAB');
          });
      } else {
        setErroLogin(true);
      };
      console.log(response);
    });
  };

  /*useEffect(() => {
    try {
      const jwt = localStorage.getItem('token');
      const decoded = jwt_decode(jwt);
      axios.get(`http://localhost:3000/usuarios/${decoded.usuario_id}`)
        .then(response => response.data)
        .then((data) => {
          setUser(data);
        });
    } catch (err) {}
  });*/

  const navigate = useNavigate();

  useEffect(() => {
    console.log(permissao);
    if (!erroLogin) {
      //const e_gestor = user['e_gestor'] ? 1 : 0;
      if (permissao == 'ADMIN') {
        console.log('ADMIN')
        navigate("/admin/menu");
      } else if (permissao == 'GESTOR') {
        console.log('GESTOR')
        navigate("/gestor/dashboard");
      } else if (permissao == 'COLAB') {
        console.log('COLAB')
        navigate("/user/menu");
      }
    }
  });

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid
        className={classes.size}
        item
        xs={12}
        sm={8}
        md={5}
        component={Paper}
        elevation={1}
        square
      >
        <div className={classes.paper}>
          <img src={logo} className={classes.imgLogo} alt='login'/>
          {erroLogin &&
            <div><Alert severity="error">Matrícula ou senha incorreta.</Alert></div>
          }
          <form className={classes.form} onSubmit={handleSubmit}>
            <TextField
              onChange={ e=>setMatricula(e.target.value)}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="matricula"
              label="Matrícula"
              name="matricula"
              autoFocus
            />
            <TextField
              onChange={ e=>setSenha(e.target.value)}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="senha"
              label="Senha"
              type="password"
              id="senha"
            />
            <Button
              style={{
                backgroundColor: "#158f42"
              }}
              type="submit"
              fullWidth
              variant="contained"
              className={classes.submit}
            >
              Entrar
            </Button>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}
