import requests

url = "https://graph.facebook.com/v4.0/me/messages"

token='DQVJzbi1raTJheGV6Ump4NE5RYUVUTTZAwNGx5dmdJOE00VTRDUHR6RjVxdTdYOHYtaTcxQW9pVHBhOVhiTW9BQkdrU3A3dUdpOU1EQ0ZAfeU1HaHloSGZAuVGFNQkctSlhMVjJHUHZAMcU82OG90dG5heE9uLW9YSTlOdExBWU8zVnNybm5vMU5kMEFoalBDRUZAvNlkyVG5PSlhqWTEtaWZAyazJ3di0yTTJ1aEdmTG5JMTRTd0tUWFlpb3ctSWFDNndpUWh3OXdn'

headers = {
    'Authorization': f'Bearer {token}',
    'Content-Type': 'application/json'
}

# Fazendo o request
def enviarMensagemWorkPedido(nome):
    data = {
        "messaging_type": "UPDATE",
        "recipient": {
            "id": 100088940215017,
        },
        "message": {
            "text": f"O funcionário {nome} acabou de fazer uma solicitação de agendamento de férias.\nVerifique: LINK"
        }
    }
    response = requests.post(url, headers=headers, json=data)
    print(response)

# Fazendo o request
def enviarMensagemWorkResposta():
    data = {
        "messaging_type": "UPDATE",
        "recipient": {
            "id": 100088940215017,
        },
        "message": {
            "text": "Seu gestor acabou de responder a sua solicitação.\nVerifique: LINK"
        }
    }
    response = requests.post(url, headers=headers, json=data)
    print(response)
