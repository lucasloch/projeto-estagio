from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import requests
import json
from typing import Optional
from enviar import enviarRelatorio, enviarPedido, enviarResposta
from workchat import enviarMensagemWorkPedido, enviarMensagemWorkResposta

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class Relatorio(BaseModel):
    id: str
    nome: str
    email: str

class FeriasMarcada(BaseModel):
    nome: str
    email: str
    inicio_ferias: str
    qtd_dias: int
    fim_ferias: str
    observacao: Optional[str]

class FeriasRespondida(BaseModel):
    email: str
    data_pedido: str
    inicio_ferias: str
    qtd_dias: float
    fim_ferias: str
    observacao: Optional[str]
    resposta: str
    mensagem: Optional[str]


@app.post("/relatorio/")
async def enviar_email(item: Relatorio):
    print(item.id)
    data1 = requests.get(f'http://localhost:3000/usuarios/relatorio/{item.id}')
    user = json.loads(data1.text)

    data2 = requests.get(f'http://localhost:3000/usuarios/saldototal/{item.id}')
    saldo = json.loads(data2.text)
    
    data3 = requests.get(f'http://localhost:3000/solicitacoes/ferias/{item.id}')
    ferias = json.loads(data3.text)
    
    data4 = requests.get(f'http://localhost:3000/usuarios/busca/ferias/{item.id}')
    precisa_ferias = json.loads(data4.text)
    
    print(user)
    relatorio = []
    achou = False
    for i in user:
        for j in saldo:
            print(j)
            if not achou:
                if i['usuario_id'] == j['usuario_usuario_id']:
                    if any(k['usuario_id'] == i['usuario_id'] for k in ferias): i['status'] = 'Férias'
                    elif any(k['usuario_id'] == i['usuario_id'] for k in precisa_ferias): 
                        for l in precisa_ferias:
                            if l['usuario_id'] == i['usuario_id']:
                                i['status'] = 'Férias atrasadas' if l['ferias_acumuladas'] else 'Regularizado'
                    else: i['status'] = 'Regularizado'
                    i['dias_usados'] = j['dias_usados']
                    i['saldo_total'] = "30"
                    relatorio.append(i)
                    achou = True
            if not achou:
                if any(k['usuario_id'] == i['usuario_id'] for k in precisa_ferias): 
                    for l in precisa_ferias:
                            if l['usuario_id'] == i['usuario_id']:
                                i['status'] = 'Férias atrasadas' if l['ferias_acumuladas'] else 'Regularizado'
                else: i['status'] = 'Regularizado'
                i['dias_usados'] = "0"
                i['saldo_total'] = "30"
                relatorio.append(i)
        achou = False
    print(relatorio)
    enviarRelatorio(relatorio, item.nome, item.email)
    return relatorio


@app.post("/solicitar/")
async def enviar_pedido(pedido: FeriasMarcada):
    print(pedido)
    enviarMensagemWorkPedido(pedido.nome)
    enviarPedido(pedido.nome, pedido.email, pedido.inicio_ferias, pedido.qtd_dias, pedido.fim_ferias, pedido.observacao)
    return pedido


@app.post("/aprovar/")
async def responder_pedido(resposta: FeriasRespondida):
    print(resposta)
    enviarMensagemWorkResposta()
    enviarResposta(resposta.email, resposta.data_pedido, resposta.inicio_ferias, resposta.qtd_dias, resposta.fim_ferias, resposta.observacao, resposta.resposta, resposta.mensagem)
    return resposta
