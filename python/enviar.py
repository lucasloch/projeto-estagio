import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
import pandas as pd
import io
from datetime import date

def export_excel(df):
  with io.BytesIO() as buffer:
    with pd.ExcelWriter(buffer) as writer:
        df.to_excel(writer, index=False)
    return buffer.getvalue()


def formatDate(date):
    datet = ''.join(date.split('T')[0])
    return '/'.join(datet.split('-')[::-1])

def enviarPedido(nome, email, inicio_ferias, qtd_dias, fim_ferias, observacao):
    sender_email = "testesdeemail7@gmail.com"
    receiver_email = email
    password = "qflxpotcenupuqmt"

    hoje = date.today()
    hojeF = hoje.strftime("%d/%m/%Y")

    message = MIMEMultipart("mixed")
    message["Subject"] = f"Pedido de Férias - {hojeF}"
    message["From"] = sender_email
    message["To"] = receiver_email

    # Criando texto da mensagem de email
    html = f"""\
    <html>
    <body>
        <p>Pedido de Férias de {nome}.</p>
        <table>
            <tr>
                <th>Data do Pedido</th>
                <th>Início das Férias</th>
                <th>Quantidade de Dias</th>
                <th>Fim das Férias</th>
                <th>Observação</th>
            </tr>
            <tr>
                <td>{hojeF}</td>
                <td>{formatDate(inicio_ferias)}</td>
                <td>{qtd_dias}</td>
                <td>{formatDate(fim_ferias)}</td>
                <td>{observacao}</td>
            </tr>
        </table>
    </body>
    </html>
    """

    # Transformando em MIMEText
    msg = MIMEText(html, "html")

    # Adicionando o texto à mensagem
    message.attach(msg)

    # Conectar ao servidor e envia o email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(
            sender_email, receiver_email, message.as_string()
        )


def enviarResposta(email, data_pedido, inicio_ferias, qtd_dias, fim_ferias, observacao, resposta, mensagemE):
    sender_email = "testesdeemail7@gmail.com"
    receiver_email = email
    password = "qflxpotcenupuqmt"

    hoje = date.today()
    hojeF = hoje.strftime("%d/%m/%Y")

    message = MIMEMultipart("mixed")
    message["Subject"] = f"Resposta a Pedido de Férias - {hojeF}"
    message["From"] = sender_email
    message["To"] = receiver_email

    # Criando texto da mensagem de email
    html = f"""\
    <html>
    <body>
        <p>Resposta ao seu pedido de férias:</p>
        <table>
            <tr>
                <th>Data do Pedido</th>
                <th>Início das Férias</th>
                <th>Quantidade de Dias</th>
                <th>Fim das Férias</th>
                <th>Observação</th>
            </tr>
            <tr>
                <td>{formatDate(data_pedido)}</td>
                <td>{formatDate(inicio_ferias)}</td>
                <td>{qtd_dias}</td>
                <td>{formatDate(fim_ferias)}</td>
                <td>{observacao}</td>
            </tr>
        </table>
        <p>Seu pedido foi: {resposta}.</p>
        <p>Mensagem: {mensagemE}</p>
    </body>
    </html>
    """

    # Transformando em MIMEText
    msg = MIMEText(html, "html")

    # Adicionando o texto à mensagem
    message.attach(msg)

    # Conectar ao servidor e envia o email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(
            sender_email, receiver_email, message.as_string()
        )


def enviarRelatorio(df, nome, email):
    # Transforma em um dataframe
    df = pd.DataFrame(df)

    # Remove os dados que foram duplicados por conta da função assíncrona
    df = df.drop_duplicates()

    # Remove a coluna "usuario_id"
    df = df.drop('usuario_id', axis=1)
    
    # Obtendo data de hoje e formatando para passar no nome do relatório
    hoje = date.today()
    hojeF = hoje.strftime("%d/%m/%Y")


    sender_email = "testesdeemail7@gmail.com"
    receiver_email = email
    password = "qflxpotcenupuqmt"

    message = MIMEMultipart("mixed")
    message["Subject"] = f"Relatório - {hojeF}"
    message["From"] = sender_email
    message["To"] = receiver_email

    # Criando texto da mensagem de email
    html = f"""\
    <html>
    <body>
        <p>Relatório emitido.</p>
        <p>Para: {nome}</p>
    </body>
    </html>
    """

    # Transformando em MIMEText
    msg = MIMEText(html, "html")

    # Adicionando o texto à mensagem
    message.attach(msg)
    message.attach(MIMEApplication(export_excel(df), Name='relatório.xlsx'))

    # Conectar ao servidor e envia o email
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(
            sender_email, receiver_email, message.as_string()
        )


