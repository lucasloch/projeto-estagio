import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuariosService } from './usuarios.service';
import { UsuariosController } from './usuarios.controller';
import { Usuario } from './entities/usuario.entity';
import { JwtModule } from '@nestjs/jwt';
import { secret } from '../constants/constants';

@Module({
  imports: [TypeOrmModule.forFeature([Usuario]),
  JwtModule.register({
    secret,
    signOptions: { expiresIn: '2h' },
  })],
  providers: [UsuariosService],
  controllers: [UsuariosController],
})
export class UsuariosModule {}