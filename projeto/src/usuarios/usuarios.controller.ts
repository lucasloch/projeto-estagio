import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Patch, UploadedFiles, Put, Req, Res } from "@nestjs/common";
import { UsuariosService } from './usuarios.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { Usuario } from './entities/usuario.entity';
import { JwtService } from '@nestjs/jwt';

@Controller('usuarios')
export class UsuariosController {
  constructor(private readonly usuariosService: UsuariosService,
    private jwtService: JwtService
  ) {}
/*
  @Post()
  create(@Body() createUsuarioDto: CreateUsuarioDto) {
    return this.usuariosService.create(createUsuarioDto);
  }
*/
  @Get()
  findAll() {
    return this.usuariosService.findAll();
  }

  @Post('/cadastro')
  async cadastro(@Res() response, @Body() createUsuarioDto: CreateUsuarioDto) {
    const newUSer = await this.usuariosService.cadastro(createUsuarioDto);
    return response.status(HttpStatus.CREATED).json({
      newUSer
    })
  }
  
  @Post('/login')
  async login(@Res() response, @Body() usuario: Usuario) {
    const token = await this.usuariosService.login(usuario, this.jwtService);
    /*if (token.hasOwnProperty('token')) {
      return response.header('Authorization', `Bearer ${token['token']}`).json();
    }*/
    return response.status(HttpStatus.OK).json(token);
  }

  @Get('/gestores')
  async buscaGestores() {
    return this.usuariosService.buscaGestores();
  }

  @Get('/areas')
  async buscaAreas() {
    return this.usuariosService.buscaAreas();
  }

  @Get('/busca/:id')
  equipe(@Param('id') id: string) {
    return this.usuariosService.equipe(+id);
  }
  
  @Get('/relatorio/:id')
  async equipeRelatorio(@Param('id') id: string) {
    return this.usuariosService.equipeRelatorio(+id);
  }

  @Get('feriasvencidas/:id')
  verificaFeriasVencidas(@Param('id') id: string) {
    return this.usuariosService.verificaFeriasVencidas(+id);
  }

  @Get('/saldototal/:id')
  saldoDiasFeriasTodos(@Param('id') id: string) {
    return this.usuariosService.saldoDiasFeriasTodos(+id);
  }

  @Get('/busca/ferias/:id')
  getPrecisaFerias(@Param('id') id: string) {
    return this.usuariosService.getPrecisaFerias(+id);
  }

  @Get('/busca/tempo/:id')
  getPrecisaFeriasUser(@Param('id') id: string) {
    return this.usuariosService.getPrecisaFeriasUser(+id);
  }

  @Patch('/desativar/:id')
  desativar(@Param('id') id: string) {
    return this.usuariosService.desativar(+id);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usuariosService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUsuarioDto: UpdateUsuarioDto) {
    return this.usuariosService.update(+id, updateUsuarioDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usuariosService.remove(+id);
  }
}
