import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Usuario } from './entities/usuario.entity';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsuariosService {
  constructor(
    @InjectRepository(Usuario)
    private usuariosRepository: Repository<Usuario>,
  ) {}
/*
  async create(dto: CreateUsuarioDto): Promise<Usuario> {
    const { matricula, nome, setor, e_gestor, tipo_regime, gmail, data_contrato, gestor_id } = dto;
    
    // check if the user exists in the db    
    const userInDb = await this.usuariosRepository.findOne({ 
        where: { matricula } 
    });/*
    if (userInDb) {
        throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);    
    }* /
    const gestor = await this.usuariosRepository.findOneBy({ gestor_id });
    
    var user: Usuario;
    if (gestor_id) {
      user = await this.usuariosRepository.create({ matricula, nome , setor, e_gestor, tipo_regime, gmail, data_contrato, gestor_id });
    } else {
      user = await this.usuariosRepository.create({ matricula, nome , setor, e_gestor, tipo_regime, gmail, data_contrato });
    }
    await this.usuariosRepository.save(user);
    return user;
  }*/

  findAll(): Promise<Usuario[]> {
    return this.usuariosRepository.find();
  }

  findOne(usuario_id: number) {
    return this.usuariosRepository.createQueryBuilder("usuario")
      .select('usuario.*')
      .addSelect("(data_contrato + INTERVAL '1 year' < current_date)", "tem1ano")
      .where("usuario_id = :usuario_id", { usuario_id })
      .execute();
  }

  update(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    return `This action updates a #${id} usuario`;
  }

  async remove(usuario_id: number) {
    return await this.usuariosRepository.delete({ usuario_id });
  }

  async desativar(usuario_id: number) {
    return await this.usuariosRepository.createQueryBuilder("usuario")
      .update()
      .set({ ativo: false })
      .where("usuario_id = :usuario_id", { usuario_id })
      .execute();
  }

  async equipeRelatorio(id: number): Promise<Usuario[]> {
    return this.usuariosRepository.createQueryBuilder("usuario")
      .select(['usuario_id', 'matricula', 'nome'])
      .addSelect("concat(to_char(data_contrato,'DD/MM/YYYY'), ' até ', to_char(((data_contrato + INTERVAL '1 year') - INTERVAL '1 day'),'DD/MM/YYYY'))", "periodo_aquisitivo")
      .where("gestor_id = :gestor_id", { gestor_id: id })
      .execute();
  }

  async equipe(id: number): Promise<Usuario[]> {
    const usuario = await this.usuariosRepository.findOne(
      { where:
        { usuario_id: id }
      }
    );
    if (usuario.e_gestor) {
      return this.usuariosRepository.createQueryBuilder("usuario")
        .where("usuario.usuario_id = :gestor_id", { gestor_id: usuario.usuario_id })
        .orWhere("usuario.gestor_id = :gestor_id", { gestor_id: usuario.usuario_id })
        .execute();
      
    } else {
      return this.usuariosRepository.createQueryBuilder("usuario")
      .where("usuario.usuario_id = :gestor_id", { gestor_id: usuario.gestor_id })
      .orWhere("usuario.gestor_id = :gestor_id", { gestor_id: usuario.gestor_id })
      .execute();
    }
  }

  async cadastro(dto: CreateUsuarioDto): Promise<Usuario> {
    const { matricula, contrato, nome, e_gestor, setor, cargo, email, gmail, data_contrato, nomeGestor } = dto;
    
    // check if the user exists in the db    
    const userInDb = await this.usuariosRepository.findOne({ 
        where: { matricula } 
    });/*
    if (userInDb) {
        throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);    
    }*/
    
    var user: Usuario;
    console.log(nomeGestor);
    if (nomeGestor) {
      const gestor = await this.usuariosRepository.findOne(
        { where:
          { nome: nomeGestor }
        }
      );
      user = await this.usuariosRepository.create({ matricula, contrato, nome, e_gestor, setor, cargo, email, gmail, data_contrato, gestor });
    } else {
      user = await this.usuariosRepository.create({ matricula, contrato, nome, e_gestor, setor, cargo, email, gmail, data_contrato });
    }
    await this.usuariosRepository.save(user);
    return user;
  }

  async login(usuario: Usuario, jwt: JwtService): Promise<{}> {
    const usuarioAchado = await this.usuariosRepository.findOneBy({ matricula: usuario.matricula });
    if (usuarioAchado) {
      const { senha } = usuarioAchado;
      if (senha == usuario.senha) {
        const payload = { usuario_id: usuarioAchado.usuario_id };
        return {
          token: jwt.sign(payload)
        };        
      };
      return new HttpException('Matrícula ou senha incorreta.', HttpStatus.UNAUTHORIZED);
    };
    return new HttpException('Matrícula ou senha incorreta.', HttpStatus.UNAUTHORIZED);
  };

  async buscaGestores(): Promise<Usuario[]> {
    return this.usuariosRepository.createQueryBuilder("usuario")
      .select(['usuario_id', 'nome'])
      .where("usuario.e_gestor = :sim", { sim: true })
      .execute();
  }

  async buscaAreas(): Promise<Usuario[]> {
    return this.usuariosRepository.createQueryBuilder("usuario")
      .select('DISTINCT (setor)')
      .execute();
  }

  async getPrecisaFerias(gestor_id: number): Promise<Usuario[]> {
    const usuario = await this.usuariosRepository.findOne({ where: {usuario_id: 1} })
    console.log(usuario.solicitacoes)
    return this.usuariosRepository.createQueryBuilder("usuario")  
      .leftJoinAndSelect("usuario.solicitacoes", "solicitacao")
      .select("usuario.usuario_id", "usuario_id")
      .addSelect("usuario.gestor_id", "gestor_id")
      .addSelect("usuario.nome", "nome")
      .addSelect("max(CASE WHEN solicitacao.solicitacao_estado = 'Aprovado' THEN solicitacao.ferias_inicio END)", "ultima_ferias")
      .addSelect("CASE WHEN (current_date > (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date) THEN (usuario.data_contrato + ((date_part('year', current_date) - date_part('year', usuario.data_contrato)) * INTERVAL '1 year'))::date ELSE usuario.data_contrato::date END", "inicio_aquisitivo")
      .addSelect("CASE WHEN (current_date > (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date) THEN (usuario.data_contrato + (((date_part('year', current_date) - date_part('year', usuario.data_contrato)) * INTERVAL '1 year')) + INTERVAL '1 year' - INTERVAL '1 day')::date ELSE (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date END", "fim_aquisitivo")
      .addSelect("CASE WHEN ((max(CASE WHEN solicitacao.solicitacao_estado = 'Aprovado' THEN solicitacao.ferias_inicio END)) NOT BETWEEN (CASE WHEN (current_date > (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date) THEN (usuario.data_contrato + ((date_part('year', current_date) - date_part('year', usuario.data_contrato)) * INTERVAL '1 year'))::date ELSE usuario.data_contrato::date END) AND (CASE WHEN (current_date > (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date) THEN (usuario.data_contrato + (((date_part('year', current_date) - date_part('year', usuario.data_contrato)) * INTERVAL '1 year')) + INTERVAL '1 year' - INTERVAL '1 day')::date ELSE (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date END)) THEN TRUE ELSE FALSE END", "ferias_acumuladas")
      .where("usuario.gestor_id = :gestor_id", { gestor_id })
      .groupBy("usuario.usuario_id")
      .orderBy("usuario.usuario_id")
      .execute();
  }

  async getPrecisaFeriasUser(id: number): Promise<Usuario[]> {
    const usuario = await this.usuariosRepository.findOne({ where: {usuario_id: 1} })
    console.log(usuario.solicitacoes)
    return this.usuariosRepository.createQueryBuilder("usuario")  
    .leftJoinAndSelect("usuario.solicitacoes", "solicitacao")
    .select("usuario.usuario_id", "usuario_id")
    .addSelect("usuario.gestor_id", "gestor_id")
    .addSelect("usuario.nome", "nome")
    .addSelect("max(CASE WHEN solicitacao.solicitacao_estado = 'Aprovado' THEN solicitacao.ferias_inicio END)", "ultima_ferias")
    .addSelect("CASE WHEN (current_date > (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date) THEN (usuario.data_contrato + ((date_part('year', current_date) - date_part('year', usuario.data_contrato)) * INTERVAL '1 year'))::date ELSE usuario.data_contrato::date END", "inicio_aquisitivo")
    .addSelect("CASE WHEN (current_date > (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date) THEN (usuario.data_contrato + (((date_part('year', current_date) - date_part('year', usuario.data_contrato)) * INTERVAL '1 year')) + INTERVAL '1 year' - INTERVAL '1 day')::date ELSE (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date END", "fim_aquisitivo")
    .addSelect("CASE WHEN ((max(CASE WHEN solicitacao.solicitacao_estado = 'Aprovado' THEN solicitacao.ferias_inicio END)) NOT BETWEEN (CASE WHEN (current_date > (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date) THEN (usuario.data_contrato + ((date_part('year', current_date) - date_part('year', usuario.data_contrato)) * INTERVAL '1 year'))::date ELSE usuario.data_contrato::date END) AND (CASE WHEN (current_date > (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date) THEN (usuario.data_contrato + (((date_part('year', current_date) - date_part('year', usuario.data_contrato)) * INTERVAL '1 year')) + INTERVAL '1 year' - INTERVAL '1 day')::date ELSE (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date END)) THEN TRUE ELSE FALSE END", "ferias_acumuladas")
    .where("usuario.usuario_id = :id", { id })
    .groupBy("usuario.usuario_id")
    .orderBy("usuario.usuario_id")
    .execute();
  }

  async saldoDiasFeriasTodos(id: number): Promise<{}> {
    return this.usuariosRepository.createQueryBuilder("usuario")
      .leftJoinAndSelect("usuario.solicitacoes", "solicitacao")
      .select(["usuario.usuario_id", "usuario.gestor_id"])
      .addSelect("COALESCE(sum(CASE WHEN solicitacao.solicitacao_estado = 'Aprovado' THEN solicitacao.quantidade_dias END), 0)", "dias_usados")
      .addSelect("CASE WHEN COALESCE(sum(CASE WHEN solicitacao.solicitacao_estado = 'Aprovado' THEN solicitacao.quantidade_dias END), 0) >= 15 THEN TRUE ELSE FALSE END", "teve_15dias")
      .where("usuario.gestor_id = :id", { id })
      .andWhere("(solicitacao.solicitacao_data BETWEEN (CASE WHEN (current_date > (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date) THEN (usuario.data_contrato + ((date_part('year', current_date) - date_part('year', usuario.data_contrato)) * INTERVAL '1 year'))::date ELSE usuario.data_contrato::date END) AND (CASE WHEN (current_date > (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date) THEN (usuario.data_contrato + (((date_part('year', current_date) - date_part('year', usuario.data_contrato)) * INTERVAL '1 year')) + INTERVAL '1 year' - INTERVAL '1 day')::date ELSE (usuario.data_contrato + INTERVAL '1 year' - INTERVAL '1 day')::date END) OR usuario.data_contrato < current_date)")
      .groupBy('usuario.usuario_id')
      .execute();
  }

  async verificaFeriasVencidas(id: number) {
    return this.usuariosRepository.createQueryBuilder("usuario")
      .leftJoinAndSelect("usuario.solicitacoes", "solicitacao")
      .select("usuario.usuario_id")
      .addSelect("CASE WHEN (max(solicitacao.ferias_inicio) IS NULL) THEN usuario.data_contrato ELSE max(solicitacao.ferias_inicio) END", "ultima")
      .addSelect("CASE WHEN (current_date > (max(solicitacao.ferias_inicio) + INTERVAL '11 months')) THEN TRUE ELSE FALSE END", "passou11meses")
      .where("usuario.usuario_id = :id", { id })
      .groupBy("usuario.usuario_id")
      .execute()
  }

}
