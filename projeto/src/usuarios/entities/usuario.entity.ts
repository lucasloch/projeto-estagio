import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { Solicitacao } from '../../solicitacoes/entities/solicitacoe.entity';

@Entity()
export class Usuario {
  @PrimaryGeneratedColumn({ type: 'int' })
  usuario_id: number;

  @Column({
    name: 'ativo',
    type: 'boolean'
  })
  ativo: boolean = true;

  @Column({
    type: 'varchar',
    unique: true,
    length: 8
  })
  matricula: string;

  @Column({ 
    name: 'contrato',
    type: 'varchar',
    length: 3
  })
  contrato: string;

  @Column({ 
    name: 'nome',
    type: 'varchar',
    length: 128
  })
  nome: string;

  @Column({ 
    name: 'data_contrato',
    type: 'date'
  })
  data_contrato: Date;

  @Column({
    name: 'e_gestor',
    type: 'boolean'
  })
  e_gestor: boolean;

  @Column({ 
    name: 'setor',
    type: 'varchar',
    length: 50
  })
  setor: string;

  @Column({ 
    name: 'cargo',
    type: 'varchar',
    length: 50,
    nullable: true
  })
  cargo: string;

  @Column({ 
    name: 'email',
    type: 'varchar',
    length: 128
  })
  email: string;

  @Column({ 
    name: 'gmail',
    type: 'varchar',
    length: 128,
    nullable: true
  })
  gmail: string;

  @Column({
    type: 'varchar',
    length: 128,
    nullable: true
  })
  senha: string = 'quero123';

  @Column({ nullable: true })
  gestor_id: number

  @ManyToOne(() => Usuario, usuario => usuario.colaboradores, {
    nullable: true
  })
  @JoinColumn({ name: 'gestor_id' })
  gestor: Usuario;

  @OneToMany(() => Usuario, usuario => usuario.gestor_id)
  colaboradores: Usuario[];

  @OneToMany(() => Solicitacao, solicitacao => solicitacao.usuario, {
    onUpdate: 'CASCADE', 
    onDelete: 'CASCADE'
  })
  solicitacoes: Solicitacao[];
}