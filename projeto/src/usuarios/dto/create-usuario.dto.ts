import { IsNotEmpty, IsString, IsBoolean, IsDate, IsNumber } from 'class-validator';
import { Usuario } from '../entities/usuario.entity';

export class CreateUsuarioDto {
  @IsNotEmpty()
  @IsString()
  readonly matricula: string;

  @IsNotEmpty()
  @IsString()
  readonly contrato: string;

  @IsNotEmpty()
  @IsString()
  readonly nome: string;

  @IsNotEmpty()
  @IsDate()
  readonly data_contrato: string;

  @IsNotEmpty()
  @IsBoolean()
  readonly e_gestor: boolean;

  @IsNotEmpty()
  @IsString()
  readonly setor: string;
  
  @IsString()
  readonly cargo: string;

  @IsNotEmpty()
  @IsString()
  readonly email: string;

  @IsString()
  readonly gmail: string;

  readonly nomeGestor: string;
}