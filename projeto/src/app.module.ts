import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuariosModule } from './usuarios/usuarios.module';
import { Usuario } from './usuarios/entities/usuario.entity';
import { SolicitacoesModule } from './solicitacoes/solicitacoes.module';
import { Solicitacao } from './solicitacoes/entities/solicitacoe.entity';
import { JwtModule } from '@nestjs/jwt';
import { secret } from './constants/constants';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: '12345',
      database: 'postgres',
      schema: 'projetonest',
      entities: [
          // __dirname + '/../**/*.entity{.ts,.js}'
          Usuario,
          Solicitacao,
      ],
      migrations: ['./migrations/*.js'],
      synchronize: true,
      logging: true,
  }),
    UsuariosModule,
    SolicitacoesModule,
    JwtModule.register({
      secret,
      signOptions: { expiresIn: '2h' },
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
