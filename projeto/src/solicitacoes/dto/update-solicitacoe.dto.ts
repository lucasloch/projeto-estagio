import { PartialType } from '@nestjs/mapped-types';
import { isNotEmpty, IsNotEmpty } from 'class-validator';
import { CreateSolicitacoeDto } from './create-solicitacoe.dto';

export class UpdateSolicitacoeDto extends PartialType(CreateSolicitacoeDto) {
    @IsNotEmpty()
    readonly solicitacao_estado: string;

    readonly mensagem: string;
}
