import { IsNotEmpty, IsString, IsBoolean, IsDate, IsNumber } from 'class-validator';
import { Usuario } from '../../usuarios/entities/usuario.entity';

export class CreateSolicitacoeDto {
  @IsDate()
  readonly ferias_inicio: string;

  @IsNumber()
  readonly quantidade_dias: number;

  @IsBoolean()
  readonly salario: boolean;

  readonly observacao: string;

  @IsNotEmpty()
  readonly usuario_id: number;

  @IsNotEmpty()
  readonly gestor_id: number;
}