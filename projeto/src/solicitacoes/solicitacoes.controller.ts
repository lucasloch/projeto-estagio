import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { SolicitacoesService } from './solicitacoes.service';
import { CreateSolicitacoeDto } from './dto/create-solicitacoe.dto';
import { UpdateSolicitacoeDto } from './dto/update-solicitacoe.dto';

@Controller('solicitacoes')
export class SolicitacoesController {
  constructor(private readonly solicitacoesService: SolicitacoesService) {}

  @Post()
  create(@Body() createSolicitacoeDto: CreateSolicitacoeDto) {
    return this.solicitacoesService.create(createSolicitacoeDto);
  }

  @Get()
  findAll() {
    return this.solicitacoesService.findAll();
  }

  @Get('/:id')
  findOne(@Param('id') id: string) {
    return this.solicitacoesService.findOne(+id)
  }

  @Get('/pedidos/:id')
  buscarPedidos(@Param('id') id: string) {
    return this.solicitacoesService.buscarPedidos(+id);
  }

  @Get('/aberto/:id')
  pedidosGestorAberto(@Param('id') id: string) {
    return this.solicitacoesService.pedidosGestorAberto(+id);
  }
  
  @Get('/resolvido/:id')
  pedidosGestorResolvido(@Param('id') id: string) {
    return this.solicitacoesService.pedidosGestorResolvido(+id);
  }

  @Get('/aprovado/:id')
  buscarPedidosAprovados(@Param('id') id: string) {
    return this.solicitacoesService.buscarPedidosAprovados(+id);
  }

  @Get('/graph/bar/:id')
  graphPedidosPorMes(@Param('id') id: string) {
    return this.solicitacoesService.graphPedidosPorMes(+id);
  }

  @Get('/saldo/:id')
  saldoDiasFerias(@Param('id') id: string) {
    return this.solicitacoesService.saldoDiasFerias(+id);
  }

  @Get('/saldototal/:id')
  saldoDiasFeriasTodos(@Param('id') id: string) {
    return this.solicitacoesService.saldoDiasFeriasTodos(+id);
  }

  @Get('/ferias/:id')
  getEstaDeFerias(@Param('id') id: string) {
    return this.solicitacoesService.getEstaDeFerias(+id);
  }

  @Get('/pedido')
  pedidoAno() {
    return this.solicitacoesService.pedidoAno();
  }

  @Get('/pedido')
  pedidoAnoAnterior() {
    return this.solicitacoesService.pedidoAnoAnterior();
  }

  @Patch('/responder/:id')
  responder(@Param('id') id: string, @Body() updateSolicitacoeDto: UpdateSolicitacoeDto) {
    return this.solicitacoesService.responder(+id, updateSolicitacoeDto);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSolicitacoeDto: UpdateSolicitacoeDto) {
    return this.solicitacoesService.update(+id, updateSolicitacoeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.solicitacoesService.remove(+id);
  }
}
