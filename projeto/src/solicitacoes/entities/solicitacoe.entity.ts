import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Usuario } from '../../usuarios/entities/usuario.entity';

@Entity()
export class Solicitacao {
  @PrimaryGeneratedColumn({ type: 'int' })
  solicitacao_id: number;
  
  @CreateDateColumn({ type: 'date' })
  solicitacao_data: Date;

  @Column({
    type: 'date',
    nullable: true
  })
  ferias_inicio: Date;

  @Column({
    type: 'int',
    nullable: true
  })
  quantidade_dias: number;

  @Column({
    type: 'date',
    nullable: true
  })
  ferias_fim: Date;
  
  @Column({
    type: 'boolean',
    default: true
  })
  salario: boolean = false;
  
  @Column({
    type: 'varchar',
    nullable: true
  })
  observacao: string;

  @Column({
    type: 'varchar',
    default: true
  })
  solicitacao_estado: string = 'Aberto';

  @Column({
    type: 'varchar',
    nullable: true
  })
  mensagem: string;

  @Column({ nullable: true })
  usuario_id: number

  @Column({ nullable: true })
  gestor_id: number

  @ManyToOne(() => Usuario, (usuario) => usuario.usuario_id)
  @JoinColumn({ name: 'usuario_id' })
  usuario: Usuario;

  @ManyToOne(() => Usuario, (usuario) => usuario.usuario_id)
  @JoinColumn({ name: 'gestor_id' })
  gestor: Usuario;
}