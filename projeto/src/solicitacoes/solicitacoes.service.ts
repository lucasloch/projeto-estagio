import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { CreateSolicitacoeDto } from './dto/create-solicitacoe.dto';
import { UpdateSolicitacoeDto } from './dto/update-solicitacoe.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Solicitacao } from './entities/solicitacoe.entity';

@Injectable()
export class SolicitacoesService {
  constructor(
    @InjectRepository(Solicitacao)
    private solicitacoesRepository: Repository<Solicitacao>,
  ) {}

  async create(dto: CreateSolicitacoeDto): Promise<Solicitacao> {

    const { ferias_inicio, quantidade_dias, salario, observacao, usuario_id, gestor_id } = dto;

    // Função para obtenção do fim das férias
    console.log(ferias_inicio);
    const addDays = function(ferias_inicio: string, quantidade_dias: number) {
      var inicio = new Date(ferias_inicio);
      inicio.setDate(inicio.getDate() + (quantidade_dias));
      return inicio.toISOString().split('T')[0];
    }
    console.log(addDays(ferias_inicio, quantidade_dias));
    
    // Realizando a soma dos dias de férias para ter a data de término
    const ferias_fim = addDays(ferias_inicio, quantidade_dias);
    const solicitacao = await this.solicitacoesRepository.create({ ferias_inicio, quantidade_dias, ferias_fim, salario, observacao, usuario_id, gestor_id });
    await this.solicitacoesRepository.save(solicitacao);

    return solicitacao;
  }

  findAll(): Promise<Solicitacao[]> {
    return this.solicitacoesRepository.find();
  }

  findOne(id: number): Promise<Solicitacao> {
    return this.solicitacoesRepository.findOneBy({ solicitacao_id: id });
  }

  async pedidosGestorAberto(gestor_id: number): Promise<Solicitacao[]> {
    return this.solicitacoesRepository.createQueryBuilder("solicitacao")
      .leftJoinAndSelect("solicitacao.usuario", "usuario")
      .where("usuario.gestor_id = :gestor_id", {gestor_id})
      .andWhere("solicitacao.solicitacao_estado = 'Aberto'")
      .execute();
  }

  async pedidosGestorResolvido(gestor_id: number): Promise<Solicitacao[]> {
    return this.solicitacoesRepository.createQueryBuilder("solicitacao")
      .leftJoinAndSelect("solicitacao.usuario", "usuario")
      .where("usuario.gestor_id = :gestor_id", {gestor_id})
      .andWhere("solicitacao.solicitacao_estado != 'Aberto'")
      .execute();
  }

  async pedidoAno(): Promise<Solicitacao[]> {
    return this.solicitacoesRepository.createQueryBuilder("solicitacao")
      .where("date_part('year', solicitacao.solicitacao_data) = date_part('year', CURRENT_DATE)")
      .execute();
  }

  async pedidoAnoAnterior(): Promise<Solicitacao[]> {
    return this.solicitacoesRepository.createQueryBuilder("solicitacao")
      .where("date_part('year', solicitacao.solicitacao_data) < date_part('year', CURRENT_DATE)")
      .execute();
  }

  async buscarPedidos(id: number): Promise<Solicitacao[]> {
    return this.solicitacoesRepository.createQueryBuilder("solicitacao")
      .where("usuario_id = :id", { id })
      .orderBy("solicitacao_data", "DESC")
      .execute();
  }

  async buscarPedidosAprovados(gestor_id: number): Promise<Solicitacao[]> {
    return this.solicitacoesRepository.createQueryBuilder("solicitacao")
      .leftJoinAndSelect("solicitacao.usuario", "usuario")
      .select("usuario.nome", "title")
      .addSelect("solicitacao.ferias_inicio", "start")
      .addSelect("solicitacao.ferias_fim", "end")
      .addSelect("solicitacao.quantidade_dias", "qtd")
      .where("usuario.gestor_id = :gestor_id", { gestor_id })
      .andWhere("solicitacao.solicitacao_estado = 'Aprovado'")
      .execute();
  }

 async  responder(id: number, updateSolicitacoeDto: UpdateSolicitacoeDto) {
    const { solicitacao_estado, mensagem=null } = updateSolicitacoeDto;
    
    this.solicitacoesRepository.createQueryBuilder("solicitacoes")
      .update()
      .set({ mensagem: mensagem })
      .where("solicitacao_id = :id", { id })
      .execute();
    
    const solicitacao = this.solicitacoesRepository.createQueryBuilder("solicitacoes")
      .update()
      .set({ solicitacao_estado: solicitacao_estado })
      .where("solicitacao_id = :id", { id })
      .execute();
    return solicitacao;
  }

  async graphPedidosPorMes(id: number): Promise<Solicitacao[]> {
    return this.solicitacoesRepository.createQueryBuilder("solicitacoes")
      .select("to_char(solicitacoes.ferias_inicio ,'Mon')", 'mes')
      .addSelect('count(*)')
      .where("gestor_id = :id", { id })
      .groupBy('1')
      .execute();
  }

  async saldoDiasFerias(id: number): Promise<{}> {
    const usuario = await this.solicitacoesRepository.createQueryBuilder("solicitacao")
      .select('usuario_id')
      .where('usuario_id = :id', { id })
      .execute();
    console.log(usuario)
    
    if(usuario.length > 0) {
      return this.solicitacoesRepository.createQueryBuilder("solicitacao")
        .select("usuario_id")
        .addSelect("sum(solicitacao.quantidade_dias) FILTER (WHERE date_part('year', solicitacao.solicitacao_data) = date_part('year', CURRENT_DATE) AND (solicitacao_estado = 'Aprovado' OR solicitacao_estado = 'Aberto'))", "dias_usados")
        .addSelect("CASE WHEN MAX(quantidade_dias) = 15 THEN TRUE ELSE FALSE END", "teve_15dias")
        .where("usuario_id = :id", { id })
        .andWhere("((solicitacao.solicitacao_estado = 'Aprovado') OR (solicitacao.solicitacao_estado = 'Aberto'))")
        .groupBy('usuario_id')
        .execute();
    }
    return new HttpException('Nao ha registros', HttpStatus.UNAUTHORIZED);
  }

  async saldoDiasFeriasTodos(id: number): Promise<{}> {
    const usuarios = await this.solicitacoesRepository.createQueryBuilder("solicitacao")
      .select('usuario_id')
      .where('gestor_id = :id', { id })
      .execute();
    console.log(usuarios)
    
    if(usuarios.length > 0) {
      return this.solicitacoesRepository.createQueryBuilder("solicitacao")
        .select("usuario_id")
        .addSelect("COALESCE(sum(solicitacao.quantidade_dias) FILTER (WHERE date_part('year', solicitacao.solicitacao_data) = date_part('year', CURRENT_DATE) AND (solicitacao_estado = 'Aprovado' OR solicitacao_estado = 'Aberto')), 0)", "dias_usados")
        .addSelect("CASE WHEN MAX(quantidade_dias) = 15 THEN TRUE ELSE FALSE END", "teve_15dias")
        .where("gestor_id = :id", { id })
        .andWhere("((solicitacao.solicitacao_estado = 'Aprovado') OR (solicitacao.solicitacao_estado = 'Aberto'))")
        .groupBy('usuario_id')
        .execute();
    }
    return new HttpException('Nao ha registros', HttpStatus.UNAUTHORIZED);
  }
  
  async getEstaDeFerias(id: number): Promise<Solicitacao[]> {
    return this.solicitacoesRepository.createQueryBuilder("solicitacao")
      .select(["usuario_id", "gestor_id"])
      .where("gestor_id = :id", { id })
      .andWhere("current_date >= solicitacao.ferias_inicio AND current_date <= solicitacao.ferias_fim")
      .execute();
  }

  update(id: number, updateSolicitacoeDto: UpdateSolicitacoeDto) {
    return `This action updates a #${id} solicitacoe`;
  }

  remove(id: number) {
    return `This action removes a #${id} solicitacoe`;
  }
}
